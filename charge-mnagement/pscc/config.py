class Config:
    # MQTT broker information
    mqtt_broker_url = "192.168.178.172"
    mqtt_port = 1883
    mqtt_username = "user"
    mqtt_password = "password"

    # MQTT Subscriptions
    mqtt_topic_p = "home/eigelbcity/e"
    mqtt_topic_p_load = "home/eigelbcity/e/P_Load"
    mqtt_topic_p_accu = "home/eigelbcity/e/P_Load"
    mqtt_topic_p_pv = "home/eigelbcity/solar/inverter/+/P_PV"
    mqtt_topic_p_charger = "home/eigelbcity/charger/HEC01/power/value"
    mqtt_topic_HEC01_state = "home/eigelbcity/charger/HEC01/chargingState/value"
    mqtt_topic_HEC02_state = "home/eigelbcity/charger/HEC02/chargingState/value"
    mqtt_topic_charger_state = "home/eigelbcity/charger/+/chargingState/value"
    mqtt_topic_charger_cmnd = "home/eigelbcity/charger/HEC01/cmnd/current"
    mqtt_topic_car_soc = "smarthome/environment/car/BLYA196E/soc"

    default_voltage = 230
