from datetime import datetime, timedelta
import time

import paho.mqtt.client as mqtt

from config import Config

import logging
from pid import PID

handle = "EnergyControl"
logger = logging.getLogger(handle)
logging.basicConfig()
logger.setLevel(logging.INFO)


def t_min_charge(charge: int,
                 current: float = 16.0,
                 phases: int = 1,
                 power: int = 0) -> int:
    """
    Calculates the minimum charging time
    :param charge: int battery capacity [Wh]
    :param current: float Charging Power [A]
    :param phases: int charging phases
    :param power: int charging power !dominant! [W]
    :return: int minimum charging time unix timestamp
    """

    # return if input is invalid
    if (phases <= 0) | (charge <= 0) | (current <= 0) | (power < 0):
        return -1

    # formula constants
    voltage = Config.default_voltage

    p = power if power else current * phases * voltage

    charge_h = charge / p

    return int(charge_h * 3600)


def t_min_charge_range(full_charge: int,
                       soc_low: float = 0,
                       soc_high: float = 100,
                       current: float = 16.0,
                       phases: int = 1,
                       power: int = 0) -> int:
    """
    Calculates the minimum charging time from soc low to soc high [s]
    :param full_charge: int battery capacity [Wh]
    :param soc_low: float soc low [%]
    :param soc_high: float soc high [%]
    :param current: float Charging Power [A]
    :param phases: int charging phases
    :param power: int charging power !dominant! [W]
    :return: int minimum charging time unix timestamp
    """

    if (phases <= 0) | (full_charge <= 0) | (current <= 0) | (soc_low < 0) | (soc_high <= 0):
        return -1

    charge_factor = abs(soc_high - soc_low) * 0.01

    return t_min_charge(int(full_charge * charge_factor), current, phases, power)


def surplus(load: int, power: int, current_mode: bool = False) -> int | float:
    """
    Calculates the Available Current [A]
    :param load: int electrical load [W]
    :param power: available power [W]
    :param current_mode: bool return as current
    :return: int | float power [W] | current [A]
    """
    voltage = Config.default_voltage

    # if (load <= 0) | (power <= 0):
    #     return -1

    # ADD operation due to negative load value if energy is consumed
    power_available = power + load

    if current_mode:
        return power_available / voltage
    return power_available


def adjust_power(load: int, power: int, old_current: float = 0) -> float:
    if old_current < 0:
        return -1

    sur_plus = surplus(load, power, current_mode=True)

    new_current = old_current + sur_plus

    if new_current:
        return new_current
    return 0


def p_to_a(p: float, p_phase: int = 1, a_phase: int = 0) -> float:
    return p * (p_phase / a_phase) / Config.default_voltage


def a_to_p(a: float, p_phase: int = 1, a_phase: int = 0) -> float:
    return a * (a_phase / p_phase) * Config.default_voltage


class EnergyManagement:
    phases = 2
    test_load = 0
    p_charge = 0
    start_time = datetime.now() - timedelta(hours=1)
    fsm = 0
    soc = 0

    def __init__(self, mqtt_client, p_load: int = 0, p_pv: int = 0, p_accu: int = 0, p_charger: int = 0):
        logger.debug("Initializing EnergyManagement")
        self.p_load = p_load
        self.p_pv = p_pv
        self.p_pv2 = 0
        self.p_charger = p_charger
        self.p_accu = p_accu
        self.hec01_state = 0
        self.hec02_state = 0
        self.hec01_priority = 0
        self.mqtt_client = mqtt_client

        self.init_mqtt()

        self.pid = PID(dt=5, _max=11000, _min=-11000, kp=0.07, ki=0.0, kd=0.6)

    def init_mqtt(self):
        logger.debug("Initializing MQTT client")
        # Assign the callbacks to the topics

        self.mqtt_client.on_message = self.on_message

        # set credentials
        self.mqtt_client.username_pw_set(Config.mqtt_username, Config.mqtt_password)

        # Connect to the broker
        self.mqtt_client.connect(Config.mqtt_broker_url, Config.mqtt_port, 3600)

        # Subscribe to the topics
        self.mqtt_client.subscribe(Config.mqtt_topic_p_load)
        self.mqtt_client.subscribe(Config.mqtt_topic_p_pv)
        self.mqtt_client.subscribe(Config.mqtt_topic_charger_state)
        self.mqtt_client.subscribe(Config.mqtt_topic_p_accu)
        self.mqtt_client.subscribe(Config.mqtt_topic_p_charger)
        self.mqtt_client.subscribe("test/power")
        self.mqtt_client.subscribe(Config.mqtt_topic_car_soc)

    def on_message(self, client, userdata, msg):
        data_f = float(msg.payload.decode())
        data_i = int(data_f)
        logger.debug(f"MQTT:sub {msg.topic}: {data_f}, qos: {msg.qos}")
        if msg.topic == Config.mqtt_topic_p_load:
            self.p_load = data_i
        elif msg.topic == Config.mqtt_topic_p_pv.replace("+", "fronius01"):
            self.p_pv = data_i
        elif msg.topic == Config.mqtt_topic_p_pv.replace("+", "fronius02"):
            logger.info(f"MQTT:sub {msg.topic}: {data_f}, qos: {msg.qos}")
            self.p_pv2 = data_i
        elif msg.topic == Config.mqtt_topic_charger_state.replace("+", "HEC01"):
            self.hec01_state = data_i
        elif msg.topic == Config.mqtt_topic_charger_state.replace("+", "HEC02"):
            self.hec02_state = data_i
        elif msg.topic == Config.mqtt_topic_charger_state:
            self.p_accu = data_i
        elif msg.topic == Config.mqtt_topic_p_charger:
            self.p_charger = data_i
        elif msg.topic == "test/power":
            self.test_load = data_i
        elif msg.topic == Config.mqtt_topic_car_soc:
            self.soc = data_i

        self.on_change()

    def on_change(self):
        # pwr = adjust_power(self.p_load, self.p_pv, self.p_charger)
        pwr = surplus(self.p_load + self.p_accu,
                      self.p_pv + self.p_charger * 1000 + self.p_pv2 + self.test_load,
                      current_mode=False) - 250
        logger.info(f"available power: {pwr:.2f} W")

        inc = self.pid.run(pwr, self.p_charge)
        self.p_charge += inc
        logger.info(f"pid power: {self.p_charge}")
        a_charge = int((self.p_charge / 230) * 10 / self.phases) if self.p_charge > 0 else 0
        a_charge = 160 if a_charge > 160 else a_charge

        if self.fsm == 0: # wait for min surplus
            if a_charge > 60:
                self.fsm = 10
                self.start_time = datetime.now()
            else:
                a_charge = 0
        elif self.fsm == 10: # charge until under limit
            if a_charge < 60:
                self.fsm = 20
        elif self.fsm == 20: # delay off
            a_charge = 60 if a_charge < 60 else a_charge
            if a_charge > 60:
                self.fsm = 10
                self.start_time = datetime.now()
            elif self.start_time + timedelta(minutes=3) < datetime.now():
                self.fsm = 0
                a_charge = 0
        else:
            self.fsm = 0

        if self.soc > 75:
            a_charge = 0

        if (self.hec01_state > 3) & (self.hec01_state < 8):
            msg = self.mqtt_client.publish(Config.mqtt_topic_charger_cmnd, a_charge)
            logger.debug(f"MQTT:pub {Config.mqtt_topic_charger_cmnd}: {a_charge}")


if __name__ == "__main__":
    # Create an MQTT client
    mq = mqtt.Client()
    em = EnergyManagement(mqtt_client=mq)

    # while True:
    #     em.on_change()
    #     time.sleep(5)

    # Start the MQTT loop
    mq.loop_forever()

    # print(t_min_charge_range(37, 0, 100, current=16, phases=2))
