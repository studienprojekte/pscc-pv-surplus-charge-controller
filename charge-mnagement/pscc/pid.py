#!/usr/bin/python3
# ------------------------------------------------------------------------------
# Python 3.4.3
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# includes
# ------------------------------------------------------------------------------
import os
import sys


# ------------------------------------------------------------------------------
class PID:
    dt = 0.0
    max = 0.0
    min = 0.0
    kp = 0.0
    kd = 0.0
    ki = 0.0
    err = 0.0
    int = 0.0

    def __init__(self, dt, _max, _min, kp, kd, ki):
        self.dt = dt
        self.max = _max
        self.min = _min
        self.kp = kp
        self.kd = kd
        self.ki = ki

    def run(self, _set, act):
        error = _set - act

        P = self.kp * error

        self.int += error * self.dt
        I = self.ki * self.int

        D = self.kd * (error - self.err) / self.dt

        output = P + I + D

        if output > self.max:
            output = self.max
        elif output < self.min:
            output = self.min

        self.err = error
        return output


# ------------------------------------------------------------------------------
# def main():
#
