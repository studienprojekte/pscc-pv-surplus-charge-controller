from apiflask import Schema
from apiflask.fields import Boolean, DateTime, Float, Integer, Nested, String
from apiflask.validators import Length, Range
from marshmallow.fields import List
from marshmallow import validates, ValidationError
import re


class TokenOutSchema(Schema):
    id = Integer()
    username = String()
    token = String()


###############
# User
###############
class AccountDataOutSchema(Schema):
    id = Integer()
    name = String()
    value = String()
    account_id = Integer()


class AccountOutSchema(Schema):
    id = String()
    uid = String()
    data = Nested(AccountDataOutSchema(many=True, exclude=['account_id']))


class HelloOutSchema(Schema):
    message = String()
    version = String()
    docs = String()


###############
# Device Data
###############
class DeviceDataOutSchema(Schema):
    id = String()
    device_id = Integer()
    name = String()
    tag = String()
    value = String()


class DeviceDataInSchema(Schema):
    name = String(required=True)
    tag = String(required=False)
    value = String(required=False)
    device_id = Integer(required=True)

    @validates("name")
    def validate_name(self, value):
        if not re.match("^[a-zA-Z1-9_]+$", value):
            raise ValidationError("Name must contain only letters, numbers and underscore.")


class DeviceDataUpdateSchema(Schema):
    id = String(required=False)
    name = String(required=False)
    tag = String(required=False)
    value = String(required=False)

    @validates("name")
    def validate_name(self, value):
        if not re.match("^[a-zA-Z0-9_]+$", value):
            raise ValidationError("Name must contain only letters, numbers and underscore.")


###############
# Device
###############
class DeviceOutSchema(Schema):
    id = String()
    name = String()
    data = Nested(DeviceDataOutSchema(many=True, exclude=['device_id']))
    tag = String()


class DeviceInSchema(Schema):
    name = String(required=True)
    data = Nested(DeviceDataInSchema(many=True, exclude=['device_id']))
    tag = String()

    @validates("name")
    def validate_name(self, value):
        if not re.match("^[a-zA-Z0-9_]+$", value):
            raise ValidationError("Name must contain only letters, numbers and underscore.")


class DeviceUpdateSchema(Schema):
    name = String(required=False)
    data = Nested(DeviceDataUpdateSchema(many=True))
    tag = String(required=False)

    @validates("name")
    def validate_name(self, value):
        if not re.match("^[a-zA-Z0-9_]+$", value):
            raise ValidationError("Name must contain only letters, numbers and underscore.")


###############
# Wallbox
###############
class WallboxOutSchema(DeviceOutSchema):
    phase = Integer()
    power_max = Integer()


class WallboxInSchema(DeviceInSchema):
    phase = Integer(validate=Range(min=1, max=3))
    power_max = Integer(validate=Range(min=0))


class WallboxUpdateSchema(DeviceUpdateSchema):
    phase = Integer(validate=Range(min=1, max=3))
    power_max = Integer(validate=Range(min=0))


###############
# Vehicle Charging Profile
###############
class ChargingProfileOutSchema(Schema):
    id = Integer()
    name = String()
    active = Boolean()

    # days = List()
    time = DateTime()
    soc_max = Float()
    range = Float()
    power_max = Integer()

    wallbox_id = Integer()

    vehicle_id = Integer()


class ChargingProfileInSchema(Schema):
    name = String(required=True, validate=Length(max=100))
    active = Boolean(required=False, load_default=False)

    days = List(Integer(required=False, validate=Range(min=0, max=1)), load_default=[0, 0, 0, 0, 0, 0, 0])
    time = DateTime(required=True)
    soc_max = Float(required=False, validate=Range(min=0.0, max=100.0), load_default=80)
    range = Float(required=False, validate=Range(min=0.0), load_default=0.0)
    power_max = Integer(required=False, validate=Range(min=0), load_default=3000)

    wallbox_id = Integer(required=True, validate=Range(min=0))
    vehicle_id = Integer(required=True, validate=Range(min=0))


class ChargingProfileUpdateSchema(Schema):
    name = String(required=True, validate=Length(max=100))
    active = Boolean(required=False)

    # days = List(required=False, Integer(validate=Range(min=0, max=1)), load_default=[0, 0, 0, 0, 0, 0, 0])
    time = DateTime(required=False)
    soc_max = Float(required=False, validate=Range(min=0.0, max=100.0))
    range = Float(required=False, validate=Range(min=0.0))
    power_max = Integer(required=False, validate=Range(min=0))

    wallbox_id = Integer(required=False, validate=Range(min=1))
    vehicle_id = Integer(required=False, validate=Range(min=1))


###############
# Vehicle
###############
class VehicleOutSchema(DeviceOutSchema):
    soc = Float()
    soc_max = Float()
    soc_min = Float()
    wallbox = String()
    consumption = Float()
    charging_power = Integer()
    range = Integer()

    charging_profiles = Nested(ChargingProfileOutSchema(many=True, exclude=['vehicle_id']))
    # charging_sessions = Nested(------(many=True, exclude=['vehicle_id']))


class VehicleInSchema(DeviceInSchema):
    soc = Float(required=False, validate=Range(min=0.0, max=100.0), load_default=0.0)
    soc_max = Float(required=False, validate=Range(min=0.0, max=100.0), load_default=0.0)
    soc_min = Float(required=False, validate=Range(min=0.0, max=100.0), load_default=0.0)
    wallbox = String(required=False, validate=Length(max=100))
    consumption = Float(required=False, validate=Range(min=0.0), load_default=0.0)
    charging_power = Integer(required=False, validate=Range(min=0), load_default=0)
    range = Integer(required=False, validate=Range(min=0.0), load_default=0)


class VehicleUpdateSchema(DeviceUpdateSchema):
    soc = Float(required=False, validate=Range(min=0.0, max=100.0))
    soc_max = Float(required=False, validate=Range(min=0.0, max=100.0))
    soc_min = Float(required=False, validate=Range(min=0.0, max=100.0))
    wallbox = String(required=False, validate=Length(max=100))
    consumption = Float(required=False, validate=Range(min=0.0))
    charging_power = Integer(required=False, validate=Range(min=0))
    range = Integer(required=False, validate=Range(min=0.0))
