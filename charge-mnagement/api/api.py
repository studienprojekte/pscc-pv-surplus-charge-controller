import threading
import time
import paho.mqtt.client as mqtt

from apiflask import APIFlask, HTTPBasicAuth, HTTPTokenAuth, abort
from flask import request
from flask_cors import CORS
from db_model import (
    Account, AccountData,
    Device, DeviceData, Wallbox, Vehicle,
    db, ChargingProfile,
)
from api_scheme import (
    HelloOutSchema,
    DeviceDataOutSchema, DeviceDataInSchema, DeviceDataUpdateSchema,
    DeviceOutSchema, DeviceInSchema, DeviceUpdateSchema,
    AccountDataOutSchema, AccountOutSchema,
    WallboxOutSchema,
    VehicleOutSchema,
    ChargingProfileInSchema, ChargingProfileOutSchema, ChargingProfileUpdateSchema,
)

from api_config import Config
from setup_tools import init_database

# def checker(client):
#     client.publish("test", time.time())
#
#
# def checker_thread():
#     print("init mqtt")
#     client = mqtt.Client("SC")
#     client.username_pw_set(Config.ADMIN_USERNAME, Config.MQTT_PASSWORD)
#     client.connect(Config.MQTT_HOST, 1883)
#
#     while True:
#         print(time.time())
#         checker(client)
#         time.sleep(5)
#
#
# print("init thread")
# x = threading.Thread(target=checker_thread)
# x.start()

api = APIFlask(__name__)
api.config.from_object(Config)
db.init_app(api)
CORS(api)
basic_auth = HTTPBasicAuth()
token_auth = HTTPTokenAuth("Bearer")


@api.cli.command('initdb')
def initdb_command():
    """Initializes the database."""
    print("Initialize database...")
    init_database(db)
    print('Initialized the database.')


# openapi.tags
api.config['TAGS'] = [
    {'name': 'Hello', 'description': 'Information about the API'},
    {'name': 'User', 'description': 'The **USER** management'},
    {'name': 'Device', 'description': 'The **DEVICE** management'},
    {'name': 'Device Data', 'description': 'The **DEVICES** data management'},
    {'name': 'Vehicle', 'description': 'The **Vehicle** management. Vehicles are Devices'},
    {'name': 'Wallbox', 'description': 'The **Wallbox** management. Wallbox are Devices'},
    {'name': 'Charging Profile', 'description': 'The **Vehicle Charging Profile** management'}
]


@api.get('/')
@api.doc(tags=['Hello'])
@api.output(HelloOutSchema)
def hello_world():
    return {
        'message': 'This is PSCC API',
        'version': '1.0.0',
        'docs': '~/docs'
    }


@api.get('/account/<string:account_id>')
@api.doc(tags=['User'])
@api.output(AccountOutSchema(many=True))
def account(account_id):
    return Account.query.get_or_404(account_id)


@api.get('/account')
@api.doc(tags=['User'])
@api.output(AccountOutSchema(many=True))
def accounts():
    return Account.query.all()


#####################################
# Charging Profile (Vehicle (Device))
#####################################
@api.get('/device/vehicle/profile/<int:_id>')
@api.doc(tags=['Device', 'Charging Profile', 'Vehicle'])
@api.output(ChargingProfileOutSchema)
def get_vehicle_charging_profile(_id):
    return ChargingProfile.query.get_or_404(_id)


@api.get('/device/vehicle/profile')
@api.doc(tags=['Device', 'Charging Profile', 'Vehicle'])
@api.output(ChargingProfileOutSchema(many=True))
def get_vehicle_charging_profiles():
    tag = request.args.get('tag')

    if tag:
        # Use the 'tag' parameter to filter the query
        filtered_charging_profiles = ChargingProfile.query.filter_by(tag=tag).all()
        return filtered_charging_profiles

    all_charging_profiles = ChargingProfile.query.all()
    return all_charging_profiles


@api.post('/device/vehicle/profile')
@api.doc(tags=['Device', 'Charging Profile', 'Vehicle'])
@api.input(ChargingProfileInSchema(), location='json')
@api.output(ChargingProfileOutSchema())
def create_vehicle_charging_profile(json_data):
    profile = ChargingProfile(**json_data)
    if ChargingProfile.unique(profile):
        db.session.add(profile)
        db.session.commit()
        return profile
    abort(422, message=f'Device "{profile.name}" already exists')


@api.patch('/device/vehicle/profile/<int:_id>')
@api.doc(tags=['Device', 'Charging Profile', 'Vehicle'])
@api.input(ChargingProfileInSchema(), location='json')
@api.output(ChargingProfileOutSchema())
def update_vehicle_charging_profile(json_data):
    None
    # profile = ChargingProfile(**json_data)
    # if ChargingProfile.unique(profile):
    #     db.session.add(profile)
    #     db.session.commit()
    #     return profile
    # abort(422, message=f'Device "{profile.name}" already exists')


@api.delete('/device/vehicle/profile/<int:_id>')
@api.doc(tags=['Device', 'Charging Profile', 'Vehicle'])
@api.output(ChargingProfileOutSchema())
def delete_vehicle_charging_profile(_id):
    profile = ChargingProfile.get_by_id(_id)

    if profile:
        db.session.delete(profile)
        db.session.commit()
        return profile
    abort(404, message=f'Vehicle Charging Profile with id: "{_id}" does not exist')


###################
# Vehicle (Device)
###################
@api.get('/device/vehicle/<string:name_id>')
@api.doc(tags=['Device', 'Vehicle'])
@api.output(VehicleOutSchema())
def get_vehicle(name_id):
    try:
        _id = int(name_id)
        print(f"Parsed integer: {_id} - get by id")
        vehicle = Vehicle.get_by_id(_id)
    except ValueError:
        print("Invalid integer format - get by name")
        vehicle = Vehicle.get_by_name(name_id)

    if vehicle:
        return vehicle
    return abort(404, message=f'vehicle "{name_id}" not found')


@api.get('/device/vehicle')
@api.doc(tags=['Device', 'Vehicle'])
@api.output(VehicleOutSchema(many=True))
def get_vehicles():
    tag = request.args.get('tag')

    if tag:
        # Use the 'tag' parameter to filter the query
        filtered_devices = Vehicle.query.filter_by(tag=tag).all()
        return filtered_devices

    all_devices = Vehicle.query.all()
    return all_devices


# @api.patch('/device/vehicle')
# @api.doc(tags=['Device', 'Vehicle'])
# @api.output(VehicleOutSchema(many=True))
# def update_vehicle():
#     tag = request.args.get('tag')
#
#     if tag:
#         # Use the 'tag' parameter to filter the query
#         filtered_devices = Vehicle.query.filter_by(tag=tag).all()
#         return filtered_devices
#
#     all_devices = Vehicle.query.all()
#     return all_devices


###################
# Wallbox (Device)
###################
@api.get('/device/wallbox/<string:name_id>')
@api.doc(tags=['Device', 'Wallbox'])
@api.output(WallboxOutSchema())
def get_wallbox(name_id):
    try:
        _id = int(name_id)
        print(f"Parsed integer: {_id} - get by id")
        wlbx = Wallbox.get_by_id(_id)
    except ValueError:
        print("Invalid integer format - get by name")
        wlbx = Wallbox.get_by_name(name_id)

    if wlbx:
        return wlbx
    return abort(404, message=f'Wallbox "{name_id}" not found')


@api.get('/device/wallbox')
@api.doc(tags=['Device', 'Wallbox'])
@api.output(WallboxOutSchema(many=True))
def get_wallbox_s():
    tag = request.args.get('tag')

    if tag:
        # Use the 'tag' parameter to filter the query
        filtered_wallbox_s = Wallbox.query.filter_by(tag=tag).all()
        return filtered_wallbox_s

    all_wallbox_s = Wallbox.query.all()
    return all_wallbox_s


###############
# Device Data
###############
@api.get('/device/data/<int:_id>')
@api.doc(tags=['Device', 'Device Data'])
@api.output(DeviceDataOutSchema)
def get_device_data(_id):
    return DeviceData.query.get_or_404(_id)


@api.get('/device/data')
@api.doc(tags=['Device Data', 'Device'])
@api.output(DeviceDataOutSchema(many=True))
def get_devices_data():
    tag = request.args.get('tag')

    if tag:
        # Use the 'tag' parameter to filter the query
        filtered_device_data = DeviceData.query.filter_by(tag=tag).all()
        return filtered_device_data

    all_device_data = DeviceData.query.all()
    return all_device_data


@api.patch('/device/data/<int:_id>')
@api.doc(tags=['Device', 'Device Data'])
@api.input(DeviceDataUpdateSchema())
@api.output(DeviceDataOutSchema())
def update_device_data(_id, json_data):
    device_data = DeviceData.get_by_id(_id)
    if device_data:
        device_data.update(json_data)
        return device_data
    abort(404, message=f'Device Data with id: "{_id}" does not exist')


@api.post('/device/data')
@api.doc(tags=['Device', 'Device Data'])
@api.input(DeviceDataInSchema())
@api.output(DeviceDataOutSchema())
def create_device_data(json_data):
    device_data = DeviceData(**json_data)
    if DeviceData.unique(device_data):
        db.session.add(device_data)
        db.session.commit()
        return device_data
    abort(422, message=f'Device "{device_data.name}" already exists')


@api.delete('/device/data/<int:_id>')
@api.doc(tags=['Device', 'Device Data'])
@api.output(DeviceDataOutSchema())
def delete_device_data(_id):
    device_data = DeviceData.get_by_id(_id)
    if device_data:
        db.session.delete(device_data)
        db.session.commit()
        return device_data
    abort(404, message=f'Device Data with id: "{_id}" does not exist')


###############
# Device
###############
@api.get('/device/<string:name_id>')
@api.doc(tags=['Device'])
@api.output(DeviceOutSchema())
def get_device(name_id):
    try:
        _id = int(name_id)
        print(f"Parsed integer: {_id} - get by id")
        dev = Device.get_by_id(_id)
    except ValueError:
        print("Invalid integer format - get by name")
        dev = Device.get_by_name(name_id)

    if dev:
        return dev
    return abort(404, message=f'Device "{name_id}" not found')


@api.get('/device')
@api.doc(tags=['Device'])
@api.output(DeviceOutSchema(many=True))
def get_devices():
    tag = request.args.get('tag')

    # Check if 'tag' is provided
    if tag:
        # Use the 'tag' parameter to filter the query
        filtered_devices = Device.query.filter_by(tag=tag).all()
        return filtered_devices

    # If 'tag' is not provided, return all devices
    all_devices = Device.query.all()
    return all_devices


@api.patch('/device/<string:name_id>')
@api.doc(tags=['Device'])
@api.input(DeviceUpdateSchema, location='json')
@api.output(DeviceOutSchema)
def update_device(name_id, json_data):
    try:
        _id = int(name_id)
        print(f"Parsed integer: {_id} - get by id")
        dev = Device.get_by_id(_id)
    except ValueError:
        print("Invalid integer format - get by name")
        dev = Device.get_by_name(name_id)

    if dev:
        dev.update(json_data)
        return dev
    return abort(404, message=f'Device "{name_id}" not found')


@api.post('/device')
@api.doc(tags=['Device'])
@api.input(DeviceInSchema, location='json')
@api.output(DeviceOutSchema)
def create_device(json_data):
    dev = Device(**json_data)
    if Device.unique(dev):
        db.session.add(dev)
        db.session.commit()
        return dev
    abort(422, message=f'Device "{dev.name}" already exists')


@api.delete('/device/<string:name_id>')
@api.doc(tags=['Device'])
@api.output(DeviceOutSchema())
def delete_device(name_id):
    try:
        _id = int(name_id)
        print(f"Parsed integer: {_id} - get by id")
        dev = Device.get_by_id(_id)
    except ValueError:
        print("Invalid integer format - get by name")
        dev = Device.get_by_name(name_id)

    if dev:
        db.session.delete(dev)
        db.session.commit()
        return dev
    abort(404, message=f'Device Data with id: "{name_id}" does not exist')


if __name__ == '__main__':
    api.run(debug=True)
