from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSONB
from werkzeug.security import check_password_hash, generate_password_hash
from itsdangerous import URLSafeTimedSerializer as Serializer
from datetime import datetime
from typing import Set

from api_config import Config

db = SQLAlchemy()

token_serializer = Serializer(
    Config.SECRET_KEY, salt=Config.PASSWORD_SALT
)


def update_attr(self, kwargs, commit=True, blacklist=None, whitelist=None, unique_list=None):
    keys = [a for a in dir(self) if not (a.startswith('__') or a.startswith('_'))
            and not callable(getattr(self, a))]
    if whitelist:
        keys = whitelist

    if blacklist:
        for b in blacklist:
            if b in keys:
                keys.remove(b)
    for key in kwargs:
        if key not in keys:
            if whitelist and key not in whitelist:
                print(f'update_attr: keyword "{key}" not in whitelist')
            elif blacklist and key in blacklist:
                print(f'update_attr: keyword "{key}" in blacklist')
            else:
                print(f'update_attr: no keyword named "{key}"')
        elif unique_list and key in unique_list:
            if self.unique(kwargs[key]):
                setattr(self, key, kwargs[key])
            else:
                print(f'update: keyword: {key} is not unique')
        elif key == 'password':
            setattr(self, key, generate_password_hash(kwargs[key]))
        else:
            setattr(self, key, kwargs[key])
    if commit:
        db.session.commit()


class Device(db.Model):
    __tablename__ = 'sh_device'

    id: db.Mapped[int] = db.mapped_column(db.Integer, primary_key=True, autoincrement=True)
    name: db.Mapped[str] = db.Column(db.String(100), nullable=False, unique=True)
    data: db.Mapped[Set["DeviceData"]] = db.relationship("DeviceData",
                                                         back_populates="device",
                                                         cascade="all, delete-orphan",
                                                         enable_typechecks=False)
    tag: db.Mapped[str] = db.Column(db.String, nullable=True)

    def __init__(self, **kwargs):
        super().__init__()
        self.name = kwargs["name"]
        if kwargs['tag']:
            self.tag = kwargs["tag"]
        if kwargs["data"]:
            for data_item in kwargs["data"]:
                device_data = DeviceData(**data_item)
                if device_data:
                    self.data.add(device_data)
        db.session.add(self)
        db.session.commit()

    @staticmethod
    def get_by_id(_id):
        return Device.query.filter_by(id=_id).first()

    @staticmethod
    def get_by_name(name):
        return Device.query.filter_by(name=name).first()

    @staticmethod
    def unique(device):
        return True

    def update(self, kwargs, commit=True):
        if 'data' in kwargs:
            for data in kwargs['data']:
                if 'id' in data:
                    try:
                        device_data = DeviceData.get_by_id(data['id'])
                        device_data.update(data)
                    except ValueError:
                        print(f"Invalid device data {data}")
            kwargs.pop('data')
        update_attr(self, kwargs, commit=commit, blacklist=('id',))


class ChargingProfile(db.Model):
    __tablename__ = 'sh_charging_profile'

    id: db.Mapped[int] = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name: db.Mapped[str] = db.Column(db.String(100), nullable=False)
    active: db.Mapped[bool] = db.Column(db.Boolean)

    days: db.Mapped[Set[int]] = db.Column(db.ARRAY(db.Integer), nullable=True)
    time: db.Mapped[datetime] = db.Column(db.DateTime, nullable=True)
    soc_max: db.Mapped[int] = db.Column(db.Integer, nullable=True)
    range: db.Mapped[int] = db.Column(db.Integer, nullable=True)
    power_max: db.Mapped[int] = db.Column(db.Integer, nullable=True)

    wallbox_id: db.Mapped[int] = db.mapped_column(db.ForeignKey('sh_wallbox.id'))
    wallbox: db.Mapped["Wallbox"] = db.relationship(back_populates='charging_profiles')

    vehicle_id: db.Mapped[int] = db.mapped_column(db.ForeignKey('sh_vehicle.id'))
    vehicle: db.Mapped["Vehicle"] = db.relationship(back_populates='charging_profiles')

    @staticmethod
    def unique(profile):
        return True
        # return profile.id not in [a.id for a in ChargingProfile.query.all()]

    @staticmethod
    def get_by_id(_id):
        return ChargingProfile.query.filter_by(id=_id).first()


class ChargingSession(db.Model):
    __tablename__ = 'sh_charging_session'

    id: db.Mapped[int] = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name: db.Mapped[str] = db.Column(db.String(100), nullable=False)
    
    start_time: db.DateTime = db.Column(db.DateTime, nullable=True)
    end_time: db.DateTime = db.Column(db.DateTime, nullable=True)

    energy: db.Mapped[int] = db.Column(db.Integer, nullable=True)

    wallbox_id: db.Mapped[int] = db.mapped_column(db.ForeignKey('sh_wallbox.id'))
    wallbox: db.Mapped["Wallbox"] = db.relationship(back_populates='charging_sessions')

    vehicle_id: db.Mapped[int] = db.mapped_column(db.ForeignKey('sh_vehicle.id'))
    vehicle: db.Mapped["Vehicle"] = db.relationship(back_populates='charging_sessions')


class Vehicle(Device):
    __tablename__ = 'sh_vehicle'

    id: db.Mapped[int] = db.Column(db.Integer, db.ForeignKey('sh_device.id'), primary_key=True)

    soc: db.Mapped[float] = db.Column(db.Float, nullable=False)
    soc_max: db.Mapped[float] = db.Column(db.Float, nullable=False)
    soc_min: db.Mapped[float] = db.Column(db.Float, nullable=False)
    wallbox: db.Mapped[str] = db.Column(db.String(50), nullable=False)
    consumption: db.Mapped[float] = db.Column(db.Float, nullable=False)  # kWh/100km
    charging_power: db.Mapped[int] = db.Column(db.Integer, nullable=False)  # W
    range: db.Mapped[int] = db.Column(db.Integer, nullable=False)

    charging_profiles: db.Mapped[Set["ChargingProfile"]] = db.relationship(back_populates="vehicle")
    charging_sessions: db.Mapped[Set["ChargingSession"]] = db.relationship(back_populates="vehicle")

    @staticmethod
    def get_by_id(_id):
        return Vehicle.query.filter_by(id=_id).first()

    @staticmethod
    def get_by_name(name):
        return Vehicle.query.filter_by(name=name).first()


class Wallbox(Device):
    __tablename__ = 'sh_wallbox'

    id: db.Mapped[int] = db.Column(db.Integer, db.ForeignKey('sh_device.id'), primary_key=True)

    phase: db.Mapped[int] = db.Column(db.Integer, nullable=True)
    power_max: db.Mapped[int] = db.Column(db.Integer, nullable=True)

    charging_profiles: db.Mapped[Set["ChargingProfile"]] = db.relationship(back_populates="vehicle")
    charging_sessions: db.Mapped[Set["ChargingSession"]] = db.relationship(back_populates="vehicle")

    @staticmethod
    def get_by_id(_id):
        return Wallbox.query.filter_by(id=_id).first()

    @staticmethod
    def get_by_name(name):
        return Wallbox.query.filter_by(name=name).first()


class DeviceData(db.Model):
    __tablename__ = 'sh_device_data'

    id: db.Mapped[int] = db.mapped_column(db.Integer, primary_key=True, autoincrement=True)
    device_id: db.Mapped[int] = db.mapped_column(db.ForeignKey("sh_device.id"))
    device: db.Mapped["Device"] = db.relationship('Device',
                                                  back_populates="data",
                                                  enable_typechecks=False)
    name: db.Mapped[str] = db.Column(db.String(100), nullable=True)
    value: db.Mapped[str] = db.Column(db.String, nullable=True)
    tag: db.Mapped[str] = db.Column(db.String, nullable=True)

    @staticmethod
    def unique(device_data):
        return (device_data.name not in [
            d.name for d in DeviceData.query.filter_by(device_id=device_data.device_id).all()
        ])

    @staticmethod
    def get_by_id(_id):
        return DeviceData.query.filter_by(id=_id).first()

    def update(self, kwargs, commit=True):
        update_attr(self, kwargs, commit=commit, blacklist=('id',))


class AccountData(db.Model):
    __tablename__ = 'sh_user_data'

    id: db.Mapped[int] = db.mapped_column(db.Integer(), primary_key=True, autoincrement=True)
    account_id: db.Mapped[int] = db.mapped_column(db.ForeignKey("sh_account.id"))
    account: db.Mapped["Account"] = db.relationship(back_populates="data")
    name: db.Mapped[str] = db.Column(db.String(100), nullable=True)
    value: db.Mapped[str] = db.Column(db.String(100), nullable=True)


class Account(db.Model):
    __tablename__ = 'sh_account'

    id: db.Mapped[int] = db.mapped_column(db.String(100), primary_key=True)
    data: db.Mapped[Set["AccountData"]] = db.relationship(back_populates="account")

    # def __init__(self, **kwargs):
    #     super().__init__()
    #     self.id = kwargs["id"]
    #     if kwargs["data"]:
    #         for data_item in kwargs["data"]:
    #             for key, value in data_item.items():
    #                 account_data = AccountData(name=key, value=value)
    #                 db.session.add(account_data)
    #
    #     db.session.add(self)
    #     db.session.commit()

    @staticmethod
    def unique(account):
        return account.id not in [a.id for a in Account.query.all()]

    def set_password(self, password):
        self.data.password = generate_password_hash(password)
        db.session().commit()

    def check_password(self, password):
        return check_password_hash(self.data.password, password)

    def generate_token(self):
        user = {"id": self.id, "username": self.username}
        self.data.token = token_serializer.dumps(user).decode("utf-8")
        return self.data.token

    def update_last_login(self):
        self.data.last_login = datetime.utcnow()
        db.session().commit()
