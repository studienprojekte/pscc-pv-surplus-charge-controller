class Config:
    SECRET_KEY = "change"
    PASSWORD_SALT = "change"
    API_PORT = 5001

    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:password@192.168.178.172/smart_home'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    ADMIN_USERNAME = "admin"
    ADMIN_PASSWORD = "password"

    AUTH_TOKEN_EXPIRATION_TIME = 3600

    MQTT_HOST = "192.168.178.172"
    MQTT_USER = "user"
    MQTT_PASSWORD = "password"
