from api_config import Config as Config
from datetime import datetime, timedelta
from db_model import (
    Account, AccountData,
    Device, DeviceData,
    Vehicle, Wallbox
)

import logging

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


def init_database(db):
    db.create_all()

    accounts = [
        {
            "id": Config.ADMIN_USERNAME,
            "data": [
                {"name": "password", "value": Config.ADMIN_PASSWORD},
                {"name": "displayname", "value": "Admin"},
                {"name": "date_added", "value": datetime.utcnow().isoformat()},
                {"name": "last_login", "value": None},
                {"name": "active", "value": str(True)}
            ]
        },
        {
            "id": "jonas",
            "data": [
                {"name": "password", "value": "password"},
                {"name": "displayname", "value": "Jonas"},
                {"name": "date_added", "value": datetime.utcnow().isoformat()},
                {"name": "last_login", "value": None},
                {"name": "active", "value": str(True)}
            ]
        }
    ]

    if Account.query.first() is None:
        cnt = 0
        logger.info(f"Creating accounts ...")
        for acc in accounts:
            account = Account(id=acc["id"])
            if not Account.unique(account):
                logger.info("Account not unique: skipping")
                continue

            cnt += 1
            logger.info(f"Creating Account {cnt}")
            logger.debug(acc)

            for d in acc["data"]:
                logger.debug(d)
                data = AccountData(name=d["name"], value=d["value"])
                account.data.add(data)
            db.session.add(account)

        trail = "s" if cnt > 1 else ""
        logger.info(f"Added {cnt} Account{trail}")
    else:
        logger.info(f"No Account added")

    wallbox_s = [
        {
            "name": "HEC01",
            "tag": "wallbox",
            "wallbox": {
                "phase": 3,
                "power_max": 11040
            },
            "data": [
                {'name': 'description', 'value': 'Heildelberger Energy Control Garage Blau', 'tag': 'info'},
                {'name': 'date_added', 'value': '', 'tag': 'info'},
                {'name': 'consumer', 'value': 'True', 'tag': 'info'},
                {'name': 'manufacturer', 'value': 'Heildelberger', 'tag': 'info'},
                {'name': 'model', 'value': 'Energy Control', 'tag': 'info'},
                {'name': 'serial', 'value': '', 'tag': 'info'},
                {'name': 'date_of_purchase', 'value': '', 'tag': 'info'},
                {'name': 'warranty', 'value': '', 'tag': 'info'},
                {'name': 'active', 'value': 'True', 'tag': 'info'},

                {'name': 'voltage', 'value': "230", 'tag': 'power'},
                {'name': 'current', 'value': "16", 'tag': 'power'},
                {'name': 'frequency', 'value': "50", 'tag': 'power'},
                {'name': 'phases', 'value': "3", 'tag': 'power'},

                {'name': 'average_power_consumption', 'value': "2000", 'tag': 'energy'},
                {'name': 'max_power_consumption', 'value': "11040", 'tag': 'energy'},
                {'name': 'daily_energy_consumption', 'value': "0", 'tag': 'energy'},
                {'name': 'monthly_energy_consumption', 'value': "0", 'tag': 'energy'},

                # {'name': 'temperature_current_temperature', 'value': "23.5", 'tag': 'temperature'},
                # {'name': 'temperature_target_temperature', 'value': "20.0", 'tag': 'temperature'},
                # {'name': 'temperature_operation_time', 'value': "0", 'tag': 'temperature'},
                # {'name': 'temperature_humidity', 'value': "0", 'tag': 'temperature'},

                {'name': 'room', 'value': 'Garage Blau', 'tag': 'location'},
                {'name': 'building', 'value': 'Main house', 'tag': 'location'},
                {'name': 'address', 'value': 'Buchenstraße 2, 72359 Dotternhausen DE', 'tag': 'location'},
                {'name': 'latitude', 'value': '48.225648', 'tag': 'location'},
                {'name': 'longitude', 'value': '8.786845', 'tag': 'location'}
            ]
        },
        {
            "name": "HEC02",
            "tag": "wallbox",
            "wallbox": {
                "phase": 3,
                "power_max": 11040
            },
            "data": [
                {'name': 'description', 'value': 'Heildelberger Energy Control Garage Silber', 'tag': 'info'},
                {'name': 'date_added', 'value': '', 'tag': 'info'},
                {'name': 'consumer', 'value': 'True', 'tag': 'info'},
                {'name': 'manufacturer', 'value': 'Heildelberger', 'tag': 'info'},
                {'name': 'model', 'value': 'Energy Control', 'tag': 'info'},
                {'name': 'serial', 'value': '', 'tag': 'info'},
                {'name': 'date_of_purchase', 'value': '', 'tag': 'info'},
                {'name': 'warranty', 'value': '', 'tag': 'info'},
                {'name': 'active', 'value': 'True', 'tag': 'info'},

                {'name': 'voltage', 'value': "230", 'tag': 'power'},
                {'name': 'current', 'value': "16", 'tag': 'power'},
                {'name': 'frequency', 'value': "50", 'tag': 'power'},
                {'name': 'phases', 'value': "3", 'tag': 'power'},

                {'name': 'average_power_consumption', 'value': "2000", 'tag': 'energy'},
                {'name': 'max_power_consumption', 'value': "11040", 'tag': 'energy'},
                {'name': 'daily_energy_consumption', 'value': "0", 'tag': 'energy'},
                {'name': 'monthly_energy_consumption', 'value': "0", 'tag': 'energy'},

                {'name': 'room', 'value': 'Garage Blau', 'tag': 'location'},
                {'name': 'building', 'value': 'Main house', 'tag': 'location'},
                {'name': 'address', 'value': 'Buchenstraße 2, 72359 Dotternhausen DE', 'tag': 'location'},
                {'name': 'latitude', 'value': '48.225648', 'tag': 'location'},
                {'name': 'longitude', 'value': '8.786845', 'tag': 'location'}
            ]
        },
        {
            "name": "HEC03",
            "tag": "wallbox",
            "wallbox": {
                "phase": 3,
                "power_max": 11040
            },
            "data": [
                {'name': 'description', 'value': 'Heildelberger Energy Control Garage Vorne', 'tag': 'info'},
                {'name': 'date_added', 'value': '', 'tag': 'info'},
                {'name': 'consumer', 'value': 'True', 'tag': 'info'},
                {'name': 'manufacturer', 'value': 'Heildelberger', 'tag': 'info'},
                {'name': 'model', 'value': 'Energy Control', 'tag': 'info'},
                {'name': 'serial', 'value': '', 'tag': 'info'},
                {'name': 'date_of_purchase', 'value': '', 'tag': 'info'},
                {'name': 'warranty', 'value': '', 'tag': 'info'},
                {'name': 'active', 'value': 'True', 'tag': 'info'},

                {'name': 'voltage', 'value': "230", 'tag': 'power'},
                {'name': 'current', 'value': "16", 'tag': 'power'},
                {'name': 'frequency', 'value': "50", 'tag': 'power'},
                {'name': 'phases', 'value': "3", 'tag': 'power'},

                {'name': 'average_power_consumption', 'value': "2000", 'tag': 'energy'},
                {'name': 'max_power_consumption', 'value': "11040", 'tag': 'energy'},
                {'name': 'daily_energy_consumption', 'value': "0", 'tag': 'energy'},
                {'name': 'monthly_energy_consumption', 'value': "0", 'tag': 'energy'},

                {'name': 'room', 'value': 'Garage Vorne', 'tag': 'location'},
                {'name': 'building', 'value': 'Main house', 'tag': 'location'},
                {'name': 'address', 'value': 'Schömbergerstraße 5, 72359 Dotternhausen DE', 'tag': 'location'},
                {'name': 'latitude', 'value': '48.225914', 'tag': 'location'},
                {'name': 'longitude', 'value': '8.785956', 'tag': 'location'}
            ]
        },
    ]

    if Wallbox.query.first() is None:
        cnt = 0
        logger.info("Creating Wallbox's ...")
        for dev in wallbox_s:
            wallbox = Wallbox(name=dev["name"],
                              tag=dev["tag"],
                              phase=dev["wallbox"]["phase"],
                              power_max=dev["wallbox"]["power_max"],
                              )
            if not Wallbox.unique(wallbox):
                logger.info("Wallbox not unique: skipping")
                continue

            cnt += 1
            logger.debug(f"Creating Wallbox {cnt}")
            logger.debug(dev)

            for d in dev["data"]:
                logger.debug(d)
                data = DeviceData(name=d["name"], value=d["value"], tag=d["tag"])
                wallbox.data.add(data)
            db.session.add(wallbox)

        trail = "s" if cnt > 1 else ""
        logger.info(f"Added {cnt} Devices{trail}")
    else:
        logger.info(f"No Wallbox added")

    vehicles = [
        {
            "name": "VWeUPBlue",
            "tag": "car",
            "vehicle": {
                "soc": 0,
                "soc_max": 70.0,
                "soc_min": 20.0,
                "wallbox": 'HEC01',
                "consumption": 15.5,  # kWh/100km
                "charging_power": 2760,  # W
                "range": 200,

                "latitude": 0.0,
                "longitude": 0.0,
            },
            "data": [
                {'name': 'description', 'value': 'VW eUP Blau', 'tag': 'info'},
                {'name': 'date_added', 'value': '', 'tag': 'info'},
                {'name': 'consumer', 'value': 'True', 'tag': 'info'},
                {'name': 'manufacturer', 'value': 'Volkswagen', 'tag': 'info'},
                {'name': 'model', 'value': 'eUP', 'tag': 'info'},
                {'name': 'serial', 'value': '', 'tag': 'info'},
                {'name': 'date_of_purchase', 'value': '', 'tag': 'info'},
                {'name': 'warranty', 'value': '', 'tag': 'info'},
                {'name': 'active', 'value': 'True', 'tag': 'info'},
                {'name': 'plate', 'value': 'BL YA xxx', 'tag': 'info'},

                {'name': 'voltage', 'value': "230", 'tag': 'power'},
                {'name': 'current', 'value': "16", 'tag': 'power'},
                {'name': 'frequency', 'value': "50", 'tag': 'power'},
                {'name': 'phases', 'value': "2", 'tag': 'power'},

                {'name': 'average_power_consumption', 'value': "3000", 'tag': 'energy'},
                {'name': 'max_power_consumption', 'value': "7360", 'tag': 'energy'},
                {'name': 'daily_energy_consumption', 'value': "6", 'tag': 'energy'},
                {'name': 'monthly_energy_consumption', 'value': "180", 'tag': 'energy'},
            ]
        },
        {
            "name": "VWeUPSilver",
            "tag": "car",
            "vehicle": {
                "soc": 0,
                "soc_max": 70.0,
                "soc_min": 20.0,
                "wallbox": 'HEC02',
                "consumption": 15.5,  # kWh/100km
                "charging_power": 2760,  # W
                "range": 200,

                "latitude": 0.0,
                "longitude": 0.0,
            },
            "data": [
                {'name': 'description', 'value': 'VW eUP Silber', 'tag': 'info'},
                {'name': 'date_added', 'value': '', 'tag': 'info'},
                {'name': 'consumer', 'value': 'True', 'tag': 'info'},
                {'name': 'manufacturer', 'value': 'Volkswagen', 'tag': 'info'},
                {'name': 'model', 'value': 'eUP', 'tag': 'info'},
                {'name': 'serial', 'value': '', 'tag': 'info'},
                {'name': 'date_of_purchase', 'value': '', 'tag': 'info'},
                {'name': 'warranty', 'value': '', 'tag': 'info'},
                {'name': 'active', 'value': 'True', 'tag': 'info'},
                {'name': 'plate', 'value': 'BL UP 42', 'tag': 'info'},

                {'name': 'voltage', 'value': "230", 'tag': 'power'},
                {'name': 'current', 'value': "16", 'tag': 'power'},
                {'name': 'frequency', 'value': "50", 'tag': 'power'},
                {'name': 'phases', 'value': "2", 'tag': 'power'},

                {'name': 'average_power_consumption', 'value': "3000", 'tag': 'energy'},
                {'name': 'max_power_consumption', 'value': "7360", 'tag': 'energy'},
                {'name': 'daily_energy_consumption', 'value': "6", 'tag': 'energy'},
                {'name': 'monthly_energy_consumption', 'value': "180", 'tag': 'energy'},
            ]
        },
        {
            "name": "bike01",
            "tag": "eBike",
            "vehicle": {
                "soc": 50.0,
                "soc_max": 80.0,
                "soc_min": 10.0,
                "wallbox": 'tasmota-04',
                "consumption": 0.9,  # kWh/100km
                "charging_power": 500,  # W
                "range": 100,

                "latitude": 0.0,
                "longitude": 0.0,
            },
            "data": [
                {'name': 'description', 'value': 'E-Bike Swen', 'tag': 'info'},
                {'name': 'date_added', 'value': '', 'tag': 'info'},
                {'name': 'consumer', 'value': 'True', 'tag': 'info'},
                {'name': 'manufacturer', 'value': '', 'tag': 'info'},
                {'name': 'model', 'value': '', 'tag': 'info'},
                {'name': 'serial', 'value': '', 'tag': 'info'},
                {'name': 'date_of_purchase', 'value': '', 'tag': 'info'},
                {'name': 'warranty', 'value': '', 'tag': 'info'},
                {'name': 'active', 'value': 'True', 'tag': 'info'},
                {'name': 'plate', 'value': '', 'tag': 'info'},

                {'name': 'voltage', 'value': "230", 'tag': 'power'},
                {'name': 'current', 'value': "16", 'tag': 'power'},
                {'name': 'frequency', 'value': "50", 'tag': 'power'},
                {'name': 'phases', 'value': "1", 'tag': 'power'},

                {'name': 'average_power_consumption', 'value': "200", 'tag': 'energy'},
                {'name': 'max_power_consumption', 'value': "600", 'tag': 'energy'},
                {'name': 'daily_energy_consumption', 'value': "0.5", 'tag': 'energy'},
                {'name': 'monthly_energy_consumption', 'value': "15", 'tag': 'energy'},
            ]
        },
    ]

    if Vehicle.query.first() is None:
        cnt = 0
        logger.info("Creating Vehicles ...")
        for dev in vehicles:
            vehicle = Vehicle(name=dev["name"],
                              tag=dev["tag"],
                              soc=dev["vehicle"]["soc"],
                              soc_max=dev["vehicle"]["soc_max"],
                              soc_min=dev["vehicle"]["soc_min"],
                              wallbox=dev["vehicle"]["wallbox"],
                              consumption=dev["vehicle"]["consumption"],
                              charging_power=dev["vehicle"]["charging_power"],
                              range=dev["vehicle"]["range"],
                              )
            if not Vehicle.unique(vehicle):
                logger.info("Device not unique: skipping")
                continue

            cnt += 1
            logger.debug(f"Creating Vehicle {cnt}")
            logger.debug(dev)

            for d in dev["data"]:
                logger.debug(d)
                data = DeviceData(name=d["name"], value=d["value"], tag=d["tag"])
                vehicle.data.add(data)
            db.session.add(vehicle)

        trail = "s" if cnt > 1 else ""
        logger.info(f"Added {cnt} Devices{trail}")
    else:
        logger.info(f"No Vehicle added")

    db.session.commit()
