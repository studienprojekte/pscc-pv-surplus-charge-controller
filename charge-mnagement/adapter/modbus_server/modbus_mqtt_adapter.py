import paho.mqtt.client as mqtt
from config import Config as Cfg
import logging
from pymodbus.client import ModbusTcpClient

_logger = logging.getLogger(__file__)
logging.basicConfig()
_logger.setLevel(logging.INFO)


def write_to_modbus_register(address: int, value: int, slave: int):
    # smart-complete.fritz.box
    client = ModbusTcpClient('192.168.178.172', port=502)  # Create client object
    client.connect()  # connect to device, reconnect automatically

    result = client.write_register(address=address, value=value, slave=slave)
    if result.isError():
        _logger.error(result)
    else:
        _logger.info(f"s{slave}: write hr at {address}: {value}")

    client.close()  # Disconnect device


# Callback when a message is received
def on_message(client, userdata, message):
    topic = message.topic
    payload = message.payload.decode()

    _logger.info(f"Received message on topic {topic}: {payload}")

    # Switch-like structure based on the topic
    if topic == "home/eigelbcity/charger/HEC01/cmnd/current":
        try:
            value = int(payload)
            write_to_modbus_register(0, value, 0)
        except ValueError:
            return
    elif topic == "home/eigelbcity/charger/HEC02/cmnd/current":
        try:
            value = int(payload)
            write_to_modbus_register(1, value, 0)
        except ValueError:
            return
    elif topic == "home/eigelbcity/charger/HEC03/cmnd/current":
        try:
            value = int(payload)
            write_to_modbus_register(2, value, 0)
        except ValueError:
            return


def main():
    # Create an MQTT client
    client = mqtt.Client()

    # set credentials
    client.username_pw_set(Cfg.mqtt_username, Cfg.mqtt_password)
    
    # Set the callback function for when a message is received
    client.on_message = on_message

    # Connect to the broker
    client.connect("192.168.178.172", Cfg.mqtt_port, 3600)

    # Subscribe to the specified topic with #
    client.subscribe("home/eigelbcity/charger/+/cmnd/#")

    # Start the loop to listen for messages
    client.loop_forever()


if __name__ == "__main__":
    main()
