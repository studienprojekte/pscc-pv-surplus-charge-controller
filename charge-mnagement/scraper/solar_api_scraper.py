import time
from datetime import datetime, timedelta

import requests
import json
import paho.mqtt.client as mqtt

import logging

_logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)
_logger.setLevel(logging.INFO)

# Fronius Solar API endpoint
fronius_api_url1 = "http://192.168.178.132/solar_api/v1/GetPowerFlowRealtimeData.fcgi"
fronius_api_url2 = "http://192.168.178.178/solar_api/v1/GetPowerFlowRealtimeData.fcgi"

# MQTT broker information
mqtt_broker_address = "192.168.178.172"
mqtt_topic_pv1 = "home/eigelbcity/solar/inverter/fronius01"
mqtt_topic_pv2 = "home/eigelbcity/solar/inverter/fronius02"
mqtt_topic_accu = "home/eigelbcity/accu/fronius01"
mqtt_topic_home = "home/eigelbcity"
mqtt_username = "user"
mqtt_password = "password"


def get_fronius_data(url):
    # Function to request data from Fronius API
    try:
        response = requests.get(url)
        data = response.json()
        return data
    except Exception as e:
        print(f"Error fetching data from Fronius API: {e}")
        return None


def publish_to_mqtt(payload, ts):
    # Function to publish data to MQTT
    client = mqtt.Client()
    client.username_pw_set(mqtt_username, mqtt_password)
    client.connect(mqtt_broker_address, 1883, 60)

    client.publish(mqtt_topic_home, json.dumps(payload), retain=True)

    for key, vals in payload.items():
        for k, val in vals.items():
            topic = f"{key}/{k}"
            client.publish(topic, str(val))
            _logger.debug(f"MQTT: Published {val} to {topic}")

    client.loop()

    client.disconnect()


if __name__ == "__main__":
    # Get data from Fronius API
    fail_time2 = fail_time1 = datetime.utcnow() - timedelta(minutes=10)
    while True:
        fronius_data1 = get_fronius_data(fronius_api_url1)
        fronius_data2 = get_fronius_data(fronius_api_url2)

        mqtt_payload = {}
        timestamp = 0
        if fronius_data1 is not None:
            # Extract PV power from Fronius data
            p_pv1 = fronius_data1["Body"]["Data"]["Site"]["P_PV"]
            p_accu = fronius_data1["Body"]["Data"]["Site"]["P_Akku"]
            p_grid = fronius_data1["Body"]["Data"]["Site"]["P_Grid"]
            p_load = fronius_data1["Body"]["Data"]["Site"]["P_Load"]
            soc_accu = fronius_data1["Body"]["Data"]["Inverters"]["1"]["SOC"]
            timestamp = fronius_data1["Head"]["Timestamp"]
            e_total = fronius_data1["Body"]["Data"]["Site"]["E_Total"]
            rel_autonomy = fronius_data1["Body"]["Data"]["Site"]["rel_Autonomy"]
            rel_self_consumption = fronius_data1["Body"]["Data"]["Site"]["rel_SelfConsumption"]

            # Create a payload for MQTT
            mqtt_payload = {
                mqtt_topic_pv1: {
                    "P_PV": p_pv1,
                },
                mqtt_topic_accu: {
                    "P_Akku": p_accu,
                    "SOC": soc_accu
                },
                f"{mqtt_topic_home}/e": {
                    "P_Grid": p_grid,
                    "P_Load": p_load,
                    "E_Total": e_total,
                    "rel_Autonomy": rel_autonomy,
                    "rel_SelfConsumption": rel_self_consumption,
                }
            }
        elif (fail_time1 + timedelta(minutes=10)) < datetime.utcnow():
            _logger.warning("Failed to fetch data from Fronius 1 API.")
            fail_time1 = datetime.utcnow()

        if fronius_data2 is not None:
            fail_time2 = 0
            p_pv2 = fronius_data2["Body"]["Data"]["Site"]["P_PV"]
            timestamp = fronius_data2["Head"]["Timestamp"]
            mqtt_payload[mqtt_topic_pv2] = {"P_PV": p_pv2, }
        elif (fail_time2 + timedelta(minutes=10)) < datetime.utcnow():
            _logger.warning("Failed to fetch data from Fronius 2 API.")
            fail_time2 = datetime.utcnow()

        # Publish data to MQTT
        publish_to_mqtt(mqtt_payload, timestamp)

        time.sleep(5)
