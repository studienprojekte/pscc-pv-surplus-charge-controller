import copy
import time
import json
from datetime import datetime

import paho.mqtt.client as mqtt
# import pytz
import requests
from requests.auth import HTTPBasicAuth

# import mappings
import ta_cmi_value_mapping
import ta_cmi_mapper

import logging

_logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)
_logger.setLevel(logging.INFO)

# TA CMI json API
username = 'user'
password = 'password'
ta_cmi_api_base_url = "http://192.168.178.18/INCLUDE/api.cgi"

# MQTT broker
mqtt_broker_address = "192.168.178.172"
mqtt_topic_charger = "home/eigelbcity/charger"
mqtt_username = "user"
mqtt_password = "password"


def get_api_data(param: dict) -> dict:
    """
    Function to request data from Fronius API
    :param param: json request arguments
    :return: dict response data
    """
    try:
        return requests.get(ta_cmi_api_base_url, auth=HTTPBasicAuth(username, password), params=param).json()
    except Exception as e:
        _logger.debug(f"Error fetching data from Fronius API: {e}")
        return {}


def publish_modbus_to_mqtt(load: list, ts: int, whitelist: list) -> None:
    """
    Function to publish values to MQTT Broker
    :param load: list of values to publish to MQTT
    :param ts: int timestamp of data request
    :param whitelist: list of whitelisted keys to publish to MQTT
    :return: None
    """

    # filter payload by whitelist
    load = [item for item in load if item['key'] in whitelist]

    _logger.debug(f"{len(load)} values to send")

    # Connect to the MQTT broker
    client = mqtt.Client()
    client.username_pw_set(mqtt_username, mqtt_password)
    client.connect(mqtt_broker_address, 1883, 60)

    # dump all as json
    client.publish(f"{mqtt_topic_charger}/json", json.dumps(payload), retain=True)
    # timestamp
    client.publish(f"{mqtt_topic_charger}/timestamp", ts, retain=True)

    # loop for keys
    for item in load:
        d_id = item["deviceID"]
        d_name = item["key"]
        topic = f"{mqtt_topic_charger}/{d_id}/{d_name}"
        client.publish(f"{topic}/value", item["value"], retain=True)
        client.publish(f"{topic}/unit", item["unit"]["unit"], retain=True)

    client.loop()

    client.disconnect()


if __name__ == "__main__":
    # Get data from TA CMI json API
    # CAN Node ID
    node = 42
    wl = ['chargingState', 'temperature', 'power', 'standby', 'eSinceOn', 'eTotal']
    params = {
        "jsonnode": node,
        "jsonparam": "AM"
    }

    while True:
        data = get_api_data(params)

        # local Timestamp from CMI
        timestamp = data["Header"]["Timestamp"]
        ts_data = (datetime.utcfromtimestamp(timestamp)  # .fromtimestamp(timestamp, tz=pytz.timezone('Europe/Berlin'))
                   .strftime('%Y-%m-%d %H:%M:%S %z'))

        # Check the status code of the response
        status_code = data["Status code"]
        status_msg = data["Status"]
        if status_code != 0:
            _logger.warning(f"[{ts_data}] Request failed with status {status_code}: {status_msg}")

            # request cooldown > 10 s to not overload CMI
            time.sleep(30)
            continue

        # create payload from request
        payload = []
        for d in data["Data"]["Modbus"]:
            # get information about data entry
            map_val = ta_cmi_value_mapping.value_mapping["nodes"][42]["modbus"]["I"][d["Number"]]
            payload.append({
                "deviceID": map_val["deviceID"],
                "key": map_val["name"],
                "value": d["Value"]["Value"],
                "unit": {
                    "id": d["Value"]["Unit"],
                    "unit": ta_cmi_mapper.mapping["units"][d["Value"]["Unit"]]["German"]
                }

            })

        temp_val = 0
        lb_payload = [x for x in payload if x["key"].find("LB") != -1]

        for d in lb_payload:
            _logger.debug(d)
            key = d["key"].replace("LB", "")
            target_key = key + "HB"
            target_device_id = d["deviceID"]
            temp_val = d["value"]
            matching_item = next(
                (item for item in payload if item["deviceID"] == target_device_id and item["key"] == target_key),
                None)
            _logger.debug(matching_item)
            temp_val += matching_item["value"] * 2**16

            new_elem = copy.deepcopy(matching_item)
            new_elem["value"] = temp_val
            new_elem["key"] = key
            new_elem["unit"]["id"] = 100
            new_elem["unit"]["unit"] = ta_cmi_mapper.mapping["units"]["100"]["German"]

            payload.append(new_elem)

        payload = [p for p in payload if ((p["key"].find("LB") == -1) & (p["key"].find("HB") == -1))]

        # publish data to MQTT
        publish_modbus_to_mqtt(payload, timestamp, wl)
        _logger.debug(f"[{ts_data}] data sent to MQTT")

        # request cycle time
        time.sleep(25)
