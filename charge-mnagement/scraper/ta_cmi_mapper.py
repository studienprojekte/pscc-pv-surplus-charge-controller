mapping = {
    "units": {
        "0": {
            "German": "",
            "English": ""
        },
        "1": {
            "German": "°C",
            "English": "°C"
        },
        "2": {
            "German": "W/m²",
            "English": "W/m²"
        },
        "3": {
            "German": "l/h",
            "English": "l/h"
        },
        "4": {
            "German": "Sek",
            "English": "sec"
        },
        "5": {
            "German": "Min",
            "English": "min"
        },
        "6": {
            "German": "l/Imp",
            "English": "l/Imp"
        },
        "7": {
            "German": "K",
            "English": "K"
        },
        "8": {
            "German": "%",
            "English": "%"
        },
        "10": {
            "German": "kW",
            "English": "kW"
        },
        "11": {
            "German": "kWh",
            "English": "kWh"
        },
        "12": {
            "German": "MWh",
            "English": "MWh"
        },
        "13": {
            "German": "V",
            "English": "V"
        },
        "14": {
            "German": "mA",
            "English": "mA"
        },
        "15": {
            "German": "Std",
            "English": "hr"
        },
        "16": {
            "German": "Tage",
            "English": "Days"
        },
        "17": {
            "German": "Imp",
            "English": "Imp"
        },
        "18": {
            "German": "kΩ",
            "English": "kΩ"
        },
        "19": {
            "German": "l",
            "English": "l"
        },
        "20": {
            "German": "km/h",
            "English": "km/h"
        },
        "21": {
            "German": "Hz",
            "English": "Hz"
        },
        "22": {
            "German": "l/min",
            "English": "l/min"
        },
        "23": {
            "German": "bar",
            "English": "bar"
        },
        "24": {
            "German": "",
            "English": ""
        },
        "25": {
            "German": "km",
            "English": "km"
        },
        "26": {
            "German": "m",
            "English": "m"
        },
        "27": {
            "German": "mm",
            "English": "mm"
        },
        "28": {
            "German": "m³",
            "English": "m³"
        },
        "35": {
            "German": "l/d",
            "English": "l/d"
        },
        "36": {
            "German": "m/s",
            "English": "m/s"
        },
        "37": {
            "German": "m³/min",
            "English": "m³/min"
        },
        "38": {
            "German": "m³/h",
            "English": "m³/h"
        },
        "39": {
            "German": "m³/d",
            "English": "m³/d"
        },
        "40": {
            "German": "mm/min",
            "English": "mm/min"
        },
        "41": {
            "German": "mm/h",
            "English": "mm/h"
        },
        "42": {
            "German": "mm/d",
            "English": "mm/d"
        },
        "43": {
            "German": "Aus/EIN",
            "English": "ON/OFF"
        },
        "44": {
            "German": "NEIN/JA",
            "English": "NO/YES"
        },
        "46": {
            "German": "°C",
            "English": "°C"
        },
        "50": {
            "German": "€",
            "English": "€"
        },
        "51": {
            "German": "$",
            "English": "$"
        },
        "52": {
            "German": "g/m³",
            "English": "g/m³"
        },
        "53": {
            "German": "",
            "English": ""
        },
        "54": {
            "German": "°",
            "English": "°"
        },
        "56": {
            "German": "°",
            "English": "°"
        },
        "57": {
            "German": "Sek",
            "English": "sec"
        },
        "58": {
            "German": "",
            "English": ""
        },
        "59": {
            "German": "%",
            "English": "%"
        },
        "60": {
            "German": "Uhr",
            "English": "h"
        },
        "63": {
            "German": "A",
            "English": "A"
        },
        "65": {
            "German": "mbar",
            "English": "mbar"
        },
        "66": {
            "German": "Pa",
            "English": "Pa"
        },
        "67": {
            "German": "ppm",
            "English": "ppm"
        },
        "68": {
            "German": "",
            "English": ""
        },
        "69": {
            "German": "W",
            "English": "W"
        },
        "70": {
            "German": "t",
            "English": "t"
        },
        "71": {
            "German": "kg",
            "English": "kg"
        },
        "72": {
            "German": "g",
            "English": "g"
        },
        "73": {
            "German": "cm",
            "English": "cm"
        },
        "74": {
            "German": "K",
            "English": "K"
        },
        "75": {
            "German": "lx",
            "English": "lx"
        },
        "76": {
            "German": "Bg/m³",
            "English": "Bg/m³"
        },
        "100": {
            "German": "Wh",
            "English": "Wh"
        },
    },
    "stausCode": [
        {
            "code": 0,
            "status": "OK",
            "description": ""
        },
        {
            "code": 1,
            "status": "NODE ERROR",
            "description": "Node not available"
        },
        {
            "code": 2,
            "status": "FAIL",
            "description": "Failure during the CAN-request/parameter not available for this device"
        },
        {
            "code": 3,
            "status": "SYNTAX ERROR",
            "description": "Error in the request String"
        },
        {
            "code": 4,
            "status": "TOO MANY REQUESTS",
            "description": "Only one request per minute is permitted"
        },
        {
            "code": 5,
            "status": "DEVICE NOT SUPPORTED",
            "description": "Device not supported"
        },
        {
            "code": 6,
            "status": "TOO FEW ARGUMENTS",
            "description": "jsonnode or jsonparam not set"
        },
        {
            "code": 7,
            "status": "CAN BUSY",
            "description": "CAN Bus is busy"
        }
    ],
    "SpX": [
        {
            "id": 1,
            "Description": "Total apparent power"
        },
        {
            "id": 2,
            "Description": "Apparent power L1"
        },
        {
            "id": 3,
            "Description": "Apparent power L2"
        },
        {
            "id": 4,
            "Description": "Apparent power L3"
        },
        {
            "id": 5,
            "Description": "Total real power"
        },
        {
            "id": 6,
            "Description": "Real power L1"
        },
        {
            "id": 7,
            "Description": "Real power L2"
        },
        {
            "id": "8",
            "Description": "Real power L3"
        },
        {
            "id": "9",
            "Description": "Total reactive power"
        },
        {
            "id": "10",
            "Description": "Reactive power L1"
        },
        {
            "id": "11",
            "Description": "Reactive power L2"
        },
        {
            "id": "12",
            "Description": "Reactive power L3"
        },
        {
            "id": "13",
            "Description": "Voltage L1"
        },
        {
            "id": "14",
            "Description": "Voltage L2"
        },
        {
            "id": "15",
            "Description": "Voltage L3"
        },
        {
            "id": "16",
            "Description": "Total amperage"
        },
        {
            "id": "17",
            "Description": "Amperage L1"
        },
        {
            "id": "18",
            "Description": "Amperage L2"
        },
        {
            "id": "19",
            "Description": "Amperage L3"
        },
        {
            "id": "20",
            "Description": "Total cos phi power factor"
        },
        {
            "id": "21",
            "Description": "Cos phi power factor L1"
        },
        {
            "id": "22",
            "Description": "Cos phi power factor L2"
        },
        {
            "id": "23",
            "Description": "Cos phi power factor L3"
        },
        {
            "id": "24",
            "Description": "Total phase shift"
        },
        {
            "id": "25",
            "Description": "phi phase shift L1"
        },
        {
            "id": "26",
            "Description": "phi phase shift L2"
        },
        {
            "id": "27",
            "Description": "phi phase shift L3"
        },
        {
            "id": "28",
            "Description": "Positive phase sequence (Yes/No)"
        }
    ]
}
