value_mapping = {
    "nodes": {
        42: {
            "modbus": {
                "I": {
                    1: {
                        "deviceID": "HEC01",
                        "name": "chargingState",
                        "bez": "Ladezustand",
                        "unit": 0
                    },
                    2: {
                        "deviceID": "HEC01",
                        "name": "currentL",
                        "bez": "Strom L1",
                        "unit": 63
                    },
                    3: {
                        "deviceID": "HEC01",
                        "name": "currentL2",
                        "bez": "Strom L2",
                        "unit": 63
                    },
                    4: {
                        "deviceID": "HEC01",
                        "name": "currentL3",
                        "bez": "Strom L3",
                        "unit": 63
                    },
                    5: {
                        "deviceID": "HEC01",
                        "name": "voltageL1",
                        "bez": "Spannung L1",
                        "unit": 13
                    },
                    6: {
                        "deviceID": "HEC01",
                        "name": "voltageL2",
                        "bez": "Spannung L2",
                        "unit": 13
                    },
                    7: {
                        "deviceID": "HEC01",
                        "name": "voltageL3",
                        "bez": "Spannung L3",
                        "unit": 13
                    },
                    8: {
                        "deviceID": "HEC01",
                        "name": "temperature",
                        "bez": "Temperatur",
                        "unit": 1
                    },
                    9: {
                        "deviceID": "HEC01",
                        "name": "power",
                        "bez": "Leistung",
                        "unit": 10
                    },
                    10: {
                        "deviceID": "HEC01",
                        "name": "eSinceOnHB",
                        "bez": "Energie seit AN HB",
                        "unit": 0
                    },
                    11: {
                        "deviceID": "HEC01",
                        "name": "eSinceOnLB",
                        "bez": "Energie seit AN LB",
                        "unit": 10
                    },
                    12: {
                        "deviceID": "HEC01",
                        "name": "eTotalHB",
                        "bez": "Energie ges HB",
                        "unit": 0
                    },
                    13: {
                        "deviceID": "HEC01",
                        "name": "eTotalLB",
                        "bez": "Energie ges LB",
                        "unit": 10
                    },
                    14: {
                        "deviceID": "HEC01",
                        "name": "standby",
                        "bez": "Standby",
                        "unit": 0
                    },
                    15: {
                        "deviceID": "HEC02",
                        "name": "chargingState",
                        "bez": "Ladezustand",
                        "unit": 8
                    },
                    16: {
                        "deviceID": "HEC02",
                        "name": "currentL1",
                        "bez": "Strom L1",
                        "unit": 63
                    },
                    17: {
                        "deviceID": "HEC02",
                        "name": "currentL2",
                        "bez": "Strom L2",
                        "unit": 63
                    },
                    18: {
                        "deviceID": "HEC02",
                        "name": "currentL3",
                        "bez": "Strom L3",
                        "unit": 63
                    },
                    19: {
                        "deviceID": "HEC02",
                        "name": "temperature",
                        "bez": "Temperatur",
                        "unit": 1
                    },
                    20: {
                        "deviceID": "HEC02",
                        "name": "voltageL2",
                        "bez": "Spannung L2",
                        "unit": 13
                    },
                    21: {
                        "deviceID": "HEC02",
                        "name": "voltageL3",
                        "bez": "Spannung L3",
                        "unit": 13
                    },
                    22: {
                        "deviceID": "HEC02",
                        "name": "voltageL1",
                        "bez": "Spannung L1",
                        "unit": 13
                    },
                    23: {
                        "deviceID": "HEC02",
                        "name": "power",
                        "bez": "Leistung",
                        "unit": 10
                    },
                    24: {
                        "deviceID": "HEC02",
                        "name": "standby",
                        "bez": "Standby",
                        "unit": 0
                    },
                    25: {
                        "deviceID": "HEC02",
                        "name": "eSinceOnHB",
                        "bez": "Energie seit AN HB",
                        "unit": 0
                    },
                    26: {
                        "deviceID": "HEC02",
                        "name": "eSinceOnLB",
                        "bez": "Energie seit AN LB",
                        "unit": 10
                    },
                    27: {
                        "deviceID": "HEC02",
                        "name": "eTotalHB",
                        "bez": "Energie ges HB",
                        "unit": 0
                    },
                    28: {
                        "deviceID": "HEC02",
                        "name": "eTotalLB",
                        "bez": "Energie ges LB",
                        "unit": 10
                    },
                    29: {
                        "deviceID": "HEC01",
                        "name": "modbusRegisterVersion",
                        "bez": "Modbus Register Vers.",
                        "unit": 0
                    },
                    30: {
                        "deviceID": "HEC02",
                        "name": "modbusRegisterVersion",
                        "bez": "Modbus Register Vers.",
                        "unit": 0
                    },
                    31: {
                        "deviceID": "HEC01",
                        "name": "modbusTimeout",
                        "bez": "TimeOut ModBus",
                        "unit": 0
                    },
                    32: {
                        "deviceID": "HEC02",
                        "name": "modbusTimeout",
                        "bez": "TimeOut ModBus",
                        "unit": 0
                    },
                    33: {
                        "deviceID": "HEC01",
                        "name": "currentOnError",
                        "bez": "Strom bei Fehler",
                        "unit": 63
                    },
                    34: {
                        "deviceID": "HEC02",
                        "name": "currentOnError",
                        "bez": "Strom bei Fehler",
                        "unit": 63
                    },
                    35: {
                        "deviceID": "HEC01",
                        "name": "ffffff",
                        "bez": "-",
                        "unit": 0
                    },
                },
                "O": [

                ]
            }
        },
        30: {
            "modbus": {
                "I": [
                    {
                        "id": 1,
                        "name": "heatFlowSetPoint",
                        "bez": "Sollwert VL WP Heizung",
                        "unit": 1
                    },
                    {
                        "id": 2,
                        "name": "coolFlowSetPoint",
                        "bez": "Sollwert VL WP Kühlen",
                        "unit": 1
                    },
                    {
                        "id": 3,
                        "bez": "operatingMode",
                        "unit": 0
                    },
                    {
                        "id": 4,
                        "name": "coolHeat",
                        "bez": "Raum Heizen Kühlen AN AUS",
                        "unit": 0
                    },
                    {
                        "id": 5,
                        "name": "whisperMode",
                        "bez": "Flüsterbetrieb AN AUS",
                        "unit": 0
                    },
                    {
                        "id": 6,
                        "name": "weatherDependentMode",
                        "bez": "Wetterabhängiger Betrieb",
                        "unit": 0
                    },
                    {
                        "id": 7,
                        "name": "deviceError",
                        "bez": "Gerätefehler",
                        "unit": 0
                    },
                    {
                        "id": 8,
                        "name": "deviceErrorCode",
                        "bez": "Gerätefehlercode",
                        "unit": 0
                    },
                    {
                        "id": 9,
                        "name": "deviceErrorMessage",
                        "bez": "Gerätefehler Subcode",
                        "unit": 0
                    },
                    {
                        "id": 10,
                        "name": "circulationPumpHeating",
                        "bez": "Umwälzpumpe WP Heizung",
                        "unit": 0
                    },
                    {
                        "id": 11,
                        "name": "compressor",
                        "bez": "Verdichter WP",
                        "unit": 0
                    },
                    {
                        "id": 12,
                        "name": "additionalHeating",
                        "bez": "Zusatzheizung WP Heizung",
                        "unit": 0
                    },
                    {
                        "id": 13,
                        "name": "disinfectionMode",
                        "bez": "Desinfektionsbetrieb",
                        "unit": 0
                    },
                    {
                        "id": 14,
                        "name": "defrost",
                        "bez": "Defrost AN/AUS",
                        "unit": 0
                    },
                    {
                        "id": 15,
                        "name": "warmStart",
                        "bez": "Warmstart",
                        "unit": 0
                    },
                    {
                        "id": 16,
                        "name": "operatingMode2",
                        "bez": "Betriebsart 2",
                        "unit": 0
                    },
                    {
                        "id": 17,
                        "name": "tBeforePlate",
                        "bez": "T WP vor Platten WT",
                        "unit": 1
                    },
                    {
                        "id": 18,
                        "name": "tBeforeAdditionalHeat",
                        "bez": "T WP vor Reserveheizung",
                        "unit": 1
                    },
                    {
                        "id": 19,
                        "name": "tReturn",
                        "bez": "T.WP Heizung RL",
                        "unit": 1
                    },
                    {
                        "id": 20,
                        "name": "tHotWater",
                        "bez": "T.Warmwasser WP Heizung 1",
                        "unit": 1
                    },
                    {
                        "id": 21,
                        "name": "tOutside",
                        "bez": "T.Außen WP",
                        "unit": 1
                    },
                    {
                        "id": 22,
                        "name": "tCoolant",
                        "bez": "T WP flüssig Kältemittel",
                        "unit": 1
                    },
                    {
                        "id": 23,
                        "name": "tThroughFlow",
                        "bez": "T.WP Heizung Durchfluss",
                        "unit": 22
                    },
                    {
                        "id": 24,
                        "name": "tRoom",
                        "bez": "Remocon Raumthermometer",
                        "unit": 1
                    },
                    # {
                    #     "id": 30,
                    #     "name": "",
                    #     "bez": "WB: Ladezustand 3",
                    #     "unit": 0
                    # },
                    # {
                    #     "id": 31,
                    #     "bez": "WB: Strom L1",
                    #     "unit": 63
                    # },
                    # {
                    #     "id": 32,
                    #     "bez": "WB: Strom L2",
                    #     "unit": 63
                    # },
                    # {
                    #     "id": 33,
                    #     "bez": "WB: Strom L3",
                    #     "unit": 63
                    # },
                    # {
                    #     "id": 34,
                    #     "bez": "WB: Temperatur",
                    #     "unit": 1
                    # },
                    # {
                    #     "id": 35,
                    #     "bez": "WB: Spannung L1",
                    #     "unit": 13
                    # },
                    # {
                    #     "id": 36,
                    #     "bez": "WB: Spannung L2",
                    #     "unit": 13
                    # },
                    # {
                    #     "id": 37,
                    #     "bez": "WB: Spannung L3",
                    #     "unit": 13
                    # },
                    # {
                    #     "id": 38,
                    #     "bez": "WB: Leistung",
                    #     "unit": 10
                    # },
                    # {
                    #     "id": 39,
                    #     "bez": "WB: Standby",
                    #     "unit": 0
                    # },
                    # {
                    #     "id": 40,
                    #     "bez": "WB Energie sein AN HB",
                    #     "unit": 0
                    # },
                    # {
                    #     "id": 41,
                    #     "bez": "WB: Energie sein AN LB",
                    #     "unit": 11
                    # },
                    # {
                    #     "id": 42,
                    #     "bez": "WB: Energie ges. HB",
                    #     "unit": 0
                    # },
                    # {
                    #     "id": 43,
                    #     "bez": "WB: Energie ges. LB",
                    #     "unit": 11
                    # }
                ],
                "O": [

                ]
            }
        }
    }
}
