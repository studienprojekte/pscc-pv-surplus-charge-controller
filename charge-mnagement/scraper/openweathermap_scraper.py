import logging
import time

import requests
import json
import paho.mqtt.client as mqtt

_logger = logging.getLogger(__file__)
logging.basicConfig()
_logger.setLevel(logging.INFO)

# Replace 'YOUR_API_KEY' with the actual API key you obtained from OpenWeatherMap
api_key = 'password'

# location coordinates (ES)
lat = '40.0000'
lon = '10.0000'

# OpenWeatherMap API endpoint for current and forcast weather
forecast_url = f'https://api.openweathermap.org/data/2.5/forecast?lat={lat}&lon={lon}&appid={api_key}&units=metric'
current_url = f'https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={api_key}&units=metric'
openweathermap_2_5_url = 'https://api.openweathermap.org/data/2.5/'

# MQTT broker details
mqtt_broker = '192.168.178.172'
mqtt_port = 1883
mqtt_topic_forcast = 'home/eigelbcity/weather/forecast'
mqtt_username = "user"
mqtt_password = "password"

client = mqtt.Client()


# Callback when a message is published
def on_publish(_client, userdata, mid):
    _logger.info(f'Message Published (message ID={mid})')


def mqtt_init():
    client.on_publish = on_publish
    client.username_pw_set(mqtt_username, mqtt_password)
    client.connect(mqtt_broker, mqtt_port, 60)


def publish_to_mqtt(mqtt_payload):
    result = client.publish(mqtt_topic_forcast, json.dumps(mqtt_payload), retain=True)

    # Wait for the message to be published
    result.wait_for_publish()


def get_current_weather():
    para = {'lat': lat, 'lon': lon, 'appid': api_key, 'units': 'metric', 'lang': 'de'}
    rs = requests.get(current_url, params=para)
    if rs.status_code == 200:
        return rs.json()
    else:
        _logger.warning(f'Error {rs.status_code}: Unable to fetch current weather.')


def get_forecast_weather():
    para = {'lat': lat, 'lon': lon, 'appid': api_key, 'units': 'metric'}
    rs = requests.get(forecast_url, params=para)
    if rs.status_code == 200:
        return rs.json()
    else:
        _logger.warning(f'Error {rs.status_code}: Unable to fetch weather forecast.')


if __name__ == "__main__":

    mqtt_init()
    payload = {'lat': lat, 'lon': lon, 'appid': api_key, 'units': 'metric', 'lang': 'de'}

    # Get data from Open Weather Map
    while True:
        r = requests.get(openweathermap_2_5_url + "/forecast", params=payload)
        if r.status_code == 200:
            # Parse the JSON response
            forecast_data = r.json()

            # Publish forecast data to MQTT topic
            publish_to_mqtt(forecast_data)
        else:
            _logger.warning(f'Error {r.status_code}: Unable to fetch weather forecast.')
            break

        time.sleep(3600)

    # Disconnect from the MQTT broker
    client.disconnect()
