import './App.css';
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import {Box, createTheme, ThemeProvider, useTheme} from "@mui/material";
import {About, SmartPlug, Charger, Dashboard, Device} from "./pages/pages";
// import {Footer, Header} from "./components/components";
import ScrollToTop from "./helpers/ScrollToTop.jsx";
import {ResponsiveAppBar} from "./components/component.js";

function App() {

    const theme = createTheme({
        palette: {
            mode: 'dark',
            primary: {
                main: '#03EEFF',
                dark: '#016B73',
                light: '#02A7B3',
                contrastText: '#fff',
            },
            secondary: {
                main: '#FF36B0',
                dark: '#801B57',
                light: '#BF2883',
                contrastText: '#fff',
            },
            success: {
                main: '#09E8B9',
                dark: '#07A885',
                light: '#0AF5C2',
                contrastText: '#fff',
            },
            bg: {
                main: '#073C40',
                dark: '#063033',
                light: '#16C0CC',
                contrastText: '#fff',
            },
            warning: {
                main: '#FFDD03',
                dark: '#FFAD0A',
                light: '#C5FF0A',
                contrastText: '#fff',
            },
            background: {
                default: '#073C40',
            }
        },
    });

    return (
        <ThemeProvider theme={theme}>
            <BrowserRouter>
                <ScrollToTop/>
                <ResponsiveAppBar/>
                <Content>
                    <Routes>
                        <Route index element={<><Dashboard/></>}/>
                        <Route path={"charger"} element={<><Charger/></>}/>
                        <Route path={"device"} element={<><Device/></>}/>
                        <Route path={"about"} element={<><About/></>}/>
                        <Route path={"smartplug"} element={<><SmartPlug/></>}/>
                    </Routes>
                </Content>
            </BrowserRouter>
        </ThemeProvider>
    );
}

function Content({children}) {
    const theme = useTheme();
    return (
        <Box
            sx={{
                py: "4.5rem",
                minHeight: '100vh',
                background: theme.palette.bg.dark
            }}
        >
            {children}
        </Box>
    )
}

export default App;
