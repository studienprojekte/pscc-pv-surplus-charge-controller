import {Checkbox, Typography, useTheme} from "@mui/material";

const LetterCheckbox = ({ letter, checked, onChange, value }) => {
    const theme = useTheme();

    const LetterIcon = () => {
        return (
            <Typography
                component={'span'}
                sx={{
                    fontSize: {xs: '1.25rem', sm: '1.5rem'},
                    border: '2px solid',
                    borderRadius: '50%',
                    width: {xs: '2rem', sm: '3rem'},
                    height: {xs: '2rem', sm: '3rem'},
                    display: 'flex',
                    alignItems: 'center', // Vertikal zentrieren
                    justifyContent: 'center', // Horizontal zentrieren
                    backgroundColor: 'transparent' }}>
                {letter}
            </Typography>
        )
    }

    return (
        <Checkbox
            id={"weekdayCheckbox-" + letter + value}
            value={value}
            sx={{p:0, m: 0, color: value === 7 ? theme.palette.error.light : 'inherit'}}
            onChange={onChange}
            checked={checked}
            icon={<LetterIcon/>}
            checkedIcon={<LetterIcon/>}
        />
    );
}

export default LetterCheckbox;

