import {LetterCheckbox} from "../component";
import Box from "@mui/material/Box";
import {useFormikContext} from "formik";

const weakDays = ["M", "D", "M", "D", "F", "S", "S"];

const WeekdayPicker = ({checkedDays, name}) => {
    const { setFieldValue } = useFormikContext();

    const handleChange = (e) => {
        const val = parseInt(e.target.value)
        if (checkedDays.includes(val))
            setFieldValue(name, checkedDays.filter((d) => d !== val))
                .then(r => r && typeof r === 'object' && Object.keys(r).length > 0);
        else
            setFieldValue(name, checkedDays.concat(val))
                .then(r => r && typeof r === 'object' && Object.keys(r).length > 0);
    }

    return (
        <Box
            name={name}
            value={checkedDays}
            sx={{
            display: 'flex',
            justifyContent: 'space-between',
            background: 'transparent',
            flexWrap: 'wrap',
            gap: 2,
        }}>
            {weakDays.map((weakDay, index) => {
                return (
                    <LetterCheckbox
                        value={index + 1}
                        onChange={handleChange}
                        letter={weakDay}
                        key={"WeekdayPicker-" + weakDay + index}
                        checked={!!checkedDays?.find(element => element === index + 1)}/>
                )
            })}
        </Box>
    )
}

export default WeekdayPicker;