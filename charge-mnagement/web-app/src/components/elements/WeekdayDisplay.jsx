import {Box, Typography, useTheme} from "@mui/material";

const weekDays = ["M", "D", "M", "D", "F", "S", "S"];
const WeekdayDisplay = ({checkedDays}) => {
    return (
        <Box sx={{display: 'flex', gap: 1}}>
            {weekDays.map((weekDay, index) => {
                return (
                    <Day checked={!checkedDays?.find(element => element === index + 1)}
                         text={weekDay}
                         key={"WeekdayDisplay-day-" + weekDay + index}/>
                )
            })}
        </Box>
    )
}

export default WeekdayDisplay;

const Day = ({checked, text}) => {
    const theme = useTheme();
    return (
        <>
            <Typography
                component="div"
                sx={{
                    color: checked ? theme.palette.text.secondary : theme.palette.primary.main,
                    position: 'relative',
                    display: 'inline-block',
                    '&::before': {
                        content: !checked ? '"•"' : '""', // Punkt nur, wenn 'checked' wahr ist
                        position: 'absolute',
                        color: theme.palette.primary.main,
                        fontSize: '24px', // Größe des Punktes
                        top: '-20px', // Abstand von oben
                        left: '50%', // Zentriert den Punkt horizontal
                        transform: 'translateX(-50%)', // Korrigiert die Zentrierung
                    },
                }}>
                {text}
            </Typography>
        </>
    )
}