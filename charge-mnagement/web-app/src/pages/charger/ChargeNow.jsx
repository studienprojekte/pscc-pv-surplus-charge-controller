import {
    Typography,
    Box,
    Accordion,
    AccordionDetails,
    AccordionSummary,
    FormControl,
    Input, InputAdornment, InputLabel, Select,
    Button,
    MenuItem, OutlinedInput, InputBase, TextField, useTheme
} from "@mui/material";
import {useState} from "react";

import {
    EvStationOutlined,
    ExpandMoreOutlined, Schedule
} from "@mui/icons-material";

import {LocalizationProvider, MobileTimePicker} from "@mui/x-date-pickers";
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import timezone from 'dayjs/plugin/timezone';
import {AdapterDayjs} from "@mui/x-date-pickers/AdapterDayjs";

import {Formik, Form} from 'formik';

dayjs.extend(utc)
dayjs.extend(timezone)
dayjs.tz.guess()

const chargers = [
    {id: 0, name: "Charger 1"},
    {id: 1, name: "Charger 2"},
    {id: 2, name: "Charger 3"},
]

const ChargeNow = () => {
    const theme = useTheme();
    return (
        <Box sx={{display: "grid", flexDirection: "column", gap: 3, mt: '1.5rem'}}>
            <Box sx={{display: "flex", justifyContent: "space-between"}}>
                <Typography
                    component={"span"}
                    sx={{paddingX: 3, fontWeight: "600", color: theme.palette.bg.contrastText, textTransform: 'uppercase'}}>
                    Charge Now
                </Typography>
            </Box>
            <Box sx={{borderRadius: 3, background: theme.palette.bg.main}}>
                <ChargeNowForm key={"charge-now-"}/>
            </Box>
        </Box>
    );
}

const default_profile = {
    time: dayjs('2022-05-23T12:34:56Z').utc(),
    maxChargingPower: 6000,
    maxSoC: 70,
    range: 200,
    charger: 1
}

const ChargeNowForm = () => {
    const [time, setTime] = useState(default_profile.time || dayjs('2022-05-23T12:34:56Z').utc() );

    const handleSubmit = (values) => {
        values.time = time;
        console.log(values)
    }

    return (
        <Box
            sx={{
                p: 3,
                display: "flex",
                flexDirection: "column",
            }}
        >
            <Formik onSubmit={(values, actions) => handleSubmit(values, actions)}
                    initialValues={default_profile}
            >
                {({
                      handleSubmit,
                      handleChange,
                      handleBlur,
                      values,
                      touched,
                      // isValid,
                      errors,
                  }) => (
                    <Form onSubmit={handleSubmit}>

                        {/* start charging */}
                        <Button type={"submit"} variant={"contained"} fullWidth color={"success"}
                                sx={{borderRadius: 3, marginBottom: 2}}>
                            <EvStationOutlined/>Start
                        </Button>

                        {/* settings */}
                        <Box sx={{display: "flex", gap: 2}}>
                            {/* Time Chooser */}
                            <LocalizationProvider
                                dateAdapter={AdapterDayjs}
                                localeText={{clearButtonLabel: 'Empty', todayButtonLabel: 'Now'}}
                            >
                                <MobileTimePicker
                                    slotProps={{ textField: { variant: 'outlined', size: 'small'} }}
                                    defaultValue={dayjs('2022-05-23T12:34:56Z').utc().local()}

                                    value={time.local()}
                                    onChange={(e) => {setTime(e)}}
                                    ampm={false}
                                    endAdornment={<InputAdornment position="end"><Schedule/></InputAdornment>}
                                />
                            </LocalizationProvider>

                            {/* Maximum SoC - State of Charge */}
                            <FormControl fullWidth
                                         variant={"outlined"}
                                         onBlur={handleBlur}
                                         sx={{marginBottom: 1.5}}
                                         error={touched.maxSoC && !!errors.maxSoC}
                                         size={"small"}>
                                <InputLabel>max SoC</InputLabel>
                                <OutlinedInput
                                    variant={"outlined"}
                                    size={'small'}
                                    type={"number"}
                                    id={"chargeNow-maxSoC"}
                                    endAdornment={<InputAdornment position="end">%</InputAdornment>}
                                    value={values.maxSoC}
                                    onChange={handleChange}
                                    label="max SoC"
                                />
                            </FormControl>

                        </Box>
                        <Accordion
                            elevation={0}
                            sx={{
                                background: "inherit",
                                border: 'none',
                                MuiAccordion: {
                                    styleOverrides: {
                                        root: {
                                            "&.Mui-expanded": {
                                                margin: "0px"
                                            }
                                        },
                                    }
                                },
                            }} disableGutters>
                            <AccordionSummary sx={{p: 0}}
                                              expandIcon={<ExpandMoreOutlined/>}
                                              id="panel1a-header"
                            >
                                <Typography>Advanced Options</Typography>
                            </AccordionSummary>
                            <AccordionDetails sx={{display: "flex", flexDirection: "column", p: 0}}>

                                {/* Range */}
                                <FormControl fullWidth
                                             onBlur={handleBlur}
                                             sx={{marginBottom: 1.5}}
                                             error={touched.range && !!errors.range}
                                             size={"small"}>
                                    <InputLabel>Range</InputLabel>
                                    <Input
                                        // inputProps={{readOnly: true, disableUnderline: true}}
                                        type={"number"}
                                        id={"range"}
                                        endAdornment={<InputAdornment position="end">km</InputAdornment>}
                                        value={values.range}
                                        onChange={handleChange}
                                    />
                                </FormControl>

                                {/* Charger */}
                                <FormControl fullWidth
                                             onBlur={handleBlur}
                                             sx={{marginBottom: 1.5}}
                                             error={touched.charger && !!errors.charger}
                                             size={"small"}>
                                    <Select
                                        id={"charger"}
                                        name="charger"
                                        value={values.charger}
                                        endAdornment={<InputAdornment position="end"><EvStationOutlined/></InputAdornment>}
                                        onChange={handleChange}
                                        IconComponent={() => null}
                                    >
                                        {chargers.map((charger) => {
                                            return (
                                                <MenuItem value={charger.id}
                                                          id={charger.name + charger.id}
                                                          key={charger.id}>
                                                    {charger.name}
                                                </MenuItem>
                                            )
                                        })}
                                    </Select>
                                </FormControl>

                                {/* Maximum Charging Power */}
                                <FormControl fullWidth
                                             onBlur={handleBlur}
                                             sx={{marginBottom: 1.5}}
                                             error={touched.maxChargingPower && !!errors.maxChargingPower}
                                             size={"small"}>
                                    <InputLabel>Maximum Charging Power</InputLabel>
                                    <Input
                                        // inputProps={{readOnly: true, disableUnderline: true}}
                                        type={"number"}
                                        id={"maxChargingPower"}
                                        endAdornment={<InputAdornment position="end">W</InputAdornment>}
                                        value={values.maxChargingPower}
                                        onChange={handleChange}
                                    />
                                </FormControl>

                            </AccordionDetails>
                        </Accordion>

                    </Form>
                )}
            </Formik>
        </Box>
    );
}

export default ChargeNow;
