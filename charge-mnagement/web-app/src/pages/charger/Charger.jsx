import {Tabs, Tab, Box} from '@mui/material';
import PropTypes from "prop-types";
import {useTheme} from "@mui/material";

import ChargerProfiles from "./ChargerPprofiles";
import ChargeNow from "./ChargeNow";
import Stats from './Stats'
import {useState} from "react";

function TabPanel(props) {
    const {children, value, index, ...other} = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`full-width-tabpanel-${index}`}
            aria-labelledby={`full-width-tab-${index}`}
            {...other}
        >
            {value === index && children}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
};

function Charger() {
    const [value, setValue] = useState(0);
    const theme = useTheme();


    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const vehicles = ['Car 1', 'Car 2'];

    return (
        <Box>
            <Tabs
                value={value}
                onChange={handleChange}
                variant="scrollable"
                scrollButtons="auto"
                aria-label="scrollable tabs vehicle"
            >
                {vehicles.map((vehicle) => (
                    <Tab label={vehicle} key={'tab-vehicle-' + vehicle}/>
                ))}
            </Tabs>
            <TabPanel value={value} index={0} dir={theme.direction}>
                <Box display={"grid"} gap={3}>
                    {/*<Stats/>*/}
                    <ChargeNow/>
                    <ChargerProfiles/>
                </Box>
            </TabPanel>
            <TabPanel value={value} index={1} dir={theme.direction}>
                Item Two
            </TabPanel>
        </Box>
    );
}

export default Charger;
