import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import {
    Accordion, AccordionActions,
    AccordionDetails,
    AccordionSummary,
    Checkbox, FormControl,
    FormControlLabel, Input, InputAdornment, InputLabel, Select,
    Switch, useTheme,
} from "@mui/material";
import ExpandMoreOutlined from "@mui/icons-material/ExpandMoreOutlined"
import {useState} from "react";
import {Add, EvStationOutlined, RadioButtonChecked, RadioButtonUnchecked} from "@mui/icons-material";
import {LocalizationProvider, MobileTimePicker} from "@mui/x-date-pickers";
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import timezone from 'dayjs/plugin/timezone';
import {AdapterDayjs} from "@mui/x-date-pickers/AdapterDayjs";
import Button from "@mui/material/Button";
import {Formik, Form} from 'formik';
import MenuItem from "@mui/material/MenuItem";

dayjs.extend(utc)
dayjs.extend(timezone)
dayjs.tz.guess()

const profiles = [
    {
        name: "arbeit",
        active: true,
        days: [1, 2, 3, 4, 5],
        time: new Date("2023-05-05T05:30:00Z").toISOString(),
        maxChargingPower: 1,
        maxSoC: 70,
        range: 200,
        charger: 1
    },
    {
        name: "markt",
        active: true,
        days: [6,],
        time: new Date("2023-05-05T15:30:00Z").toISOString(),
        maxLoadRate: 1,
        maxSoC: 70,
        range: 200,
        charger: 0
    }
]

const chargers = [
    {id: 0, name: "Charger 1"},
    {id: 1, name: "Charger 2"},
    {id: 2, name: "Charger 3"},
]

const ChargerProfiles = () => {
    const theme = useTheme()

    return (
        <Box sx={{display: "grid", flexDirection: "column", gap: 3}}>
            <Box sx={{display: "flex", justifyContent: "space-between", paddingX: 3, alignItems: 'end'}}>
                <Typography
                    component={"span"}
                    sx={{fontWeight: "600", color: theme.palette.bg.contrastText, textTransform: 'uppercase'}}>
                    Profiles
                </Typography>
                <Button sx={{borderRadius: 3, background: theme.palette.bg.main}}><Add/></Button>
            </Box>
            {profiles && profiles.map((profile) => {
                return (
                    <Box sx={{borderRadius: 3, background: theme.palette.bg.main}} key={"charging-profile-" + profile.name}>
                        <ChargerProfile profile={profile}/>
                    </Box>
                )
            })}
        </Box>
    );
}

const ChargerProfile = ({profile}) => {
    const [active, setActive] = useState(profile.active);
    const [time, setTime] = useState(dayjs.utc(profile.time) || dayjs("2023-05-05T07:30:00Z"));
    const [days, setDays] = useState(profile.days || []);
    const [edit, setEdit] = useState(false);

    const theme = useTheme();

    const handleSubmit = (values) => {
        console.log({time: time.toISOString(), active: active, days: days});
        console.log(values)
    }
    const handleChangeDays = (d) => {
        if (days.includes(d))
            setDays(days.filter((e) => e !== d));
        else
            setDays(days.concat(d));
    }


    return (
        <Box
            sx={{
                p: 3,
                display: "flex",
                flexDirection: "column",
            }}
        >
            <Formik onSubmit={(values, actions) => handleSubmit(values, actions)}
                    initialValues={profile}
            >
                {({
                      handleSubmit,
                      handleChange,
                      handleBlur,
                      values,
                      touched,
                      isValid,
                      errors,
                  }) => (
                    <Form onSubmit={handleSubmit}>
                        <Box sx={{display: "flex", justifyContent: "space-between"}}>
                            <Typography
                                component={"span"}
                                sx={{fontWeight: "600", color: theme.palette.bg.contrastText, textTransform: 'uppercase'}}>
                                {profile.name}
                            </Typography>
                            <Switch value={profile.active} checked={active} onChange={() => setActive(!active)}/>
                        </Box>

                        <Box sx={{display: "flex", justifyContent: "space-between"}}>
                            <Box sx={{display: "flex"}}>
                                <Box>
                                    {["MO", "DI", "MI", "DO", "FR", "SA", "SO"].map((day, index) => {
                                        return (
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                        icon={<RadioButtonUnchecked/>}
                                                        checkedIcon={<RadioButtonChecked/>}
                                                        checked={!!days?.find(element => element === index + 1)}
                                                        onChange={() => handleChangeDays(index + 1)}
                                                    />
                                                }
                                                label={day}
                                                labelPlacement="top"
                                                key={profile.name + "-day-" + day}
                                                sx={{m: 0, color: theme.palette.bg.contrastText}}
                                            />
                                        )
                                    })}
                                    <LocalizationProvider
                                        dateAdapter={AdapterDayjs}
                                        localeText={{clearButtonLabel: 'Empty', todayButtonLabel: 'Now'}}
                                    >
                                        <MobileTimePicker
                                            value={time.local()}
                                            onChange={(e) => {
                                                setTime(e)
                                            }}
                                            ampm={false}
                                        />
                                    </LocalizationProvider>
                                </Box>

                            </Box>
                        </Box>
                        <Accordion
                            elevation={0}
                            sx={{
                                background: "inherit",
                                border: 'none',
                                MuiAccordion: {
                                    styleOverrides: {
                                        root: {
                                            "&.Mui-expanded": {
                                                margin: "0px"
                                            }
                                        },
                                    }
                                },
                            }} disableGutters>
                            <AccordionSummary sx={{p: 0}}
                                              expandIcon={<ExpandMoreOutlined/>}
                                              id="panel1a-header"
                            >
                                <Typography>Advanced Options</Typography>
                            </AccordionSummary>
                            <AccordionDetails sx={{display: "flex", flexDirection: "column", p: 0}}>

                                {/* Maximum SoC - State of Charge */}
                                <FormControl fullWidth
                                             onBlur={handleBlur}
                                             sx={{marginBottom: 1.5}}
                                             error={touched.maxSoC && !!errors.maxSoC}
                                             size={"small"}>
                                    <InputLabel>Maximum SoC - State of Charge</InputLabel>
                                    <Input
                                        // inputProps={{readOnly: true, disableUnderline: true}}
                                        type={"number"}
                                        id={"maxSoC"}
                                        endAdornment={<InputAdornment position="end">%</InputAdornment>}
                                        value={values.maxSoC}
                                        onChange={handleChange}
                                    />
                                </FormControl>

                                {/* Range */}
                                <FormControl fullWidth
                                             onBlur={handleBlur}
                                             sx={{marginBottom: 1.5}}
                                             error={touched.range && !!errors.range}
                                             size={"small"}>
                                    <InputLabel>Range</InputLabel>
                                    <Input
                                        // inputProps={{readOnly: true, disableUnderline: true}}
                                        type={"number"}
                                        id={"range"}
                                        endAdornment={<InputAdornment position="end">km</InputAdornment>}
                                        value={values.range}
                                        onChange={handleChange}
                                    />
                                </FormControl>

                                {/* Charger */}
                                <FormControl fullWidth
                                             onBlur={handleBlur}
                                             sx={{marginBottom: 1.5}}
                                             error={touched.charger && !!errors.charger}
                                             size={"small"}>
                                    <Select
                                        id={"charger"}
                                        name="charger"
                                        value={values.charger}
                                        endAdornment={<EvStationOutlined/>}
                                        onChange={handleChange}
                                        IconComponent={() => null}
                                    >
                                        {chargers.map((charger) => {
                                            return (
                                                <MenuItem value={charger.id}
                                                          id={charger.name + charger.id}
                                                          key={charger.id}>{charger.name}
                                                </MenuItem>
                                            )
                                        })}
                                    </Select>
                                </FormControl>

                                {/* Maximum Charging Power */}
                                <FormControl fullWidth
                                             onBlur={handleBlur}
                                             sx={{marginBottom: 1.5}}
                                             error={touched.maxChargingPower && !!errors.maxChargingPower}
                                             size={"small"}>
                                    <InputLabel>Maximum Charging Power</InputLabel>
                                    <Input
                                        // inputProps={{readOnly: true, disableUnderline: true}}
                                        type={"number"}
                                        id={"maxChargingPower"}
                                        endAdornment={<InputAdornment position="end">W</InputAdornment>}
                                        value={values.maxChargingPower}
                                        onChange={handleChange}
                                    />
                                </FormControl>

                            </AccordionDetails>
                            {edit &&
                                <AccordionActions>
                                    <Button variant={"outlined"} fullWidth>cancel</Button>
                                    <Button type={"submit"} variant={"contained"} color={"success"}
                                            fullWidth>save</Button>
                                </AccordionActions>
                            }
                        </Accordion>

                    </Form>
                )}
            </Formik>
        </Box>
    );
}

export default ChargerProfiles;
