import {Typography, Box} from '@mui/material';
import {useState} from "react";
import ReactApexChart from 'react-apexcharts'

import td from '../../../testdata.json'

const bull = (
    <Box
        component="span"
        sx={{display: 'inline-block', mx: '2px', transform: 'scale(0.8)'}}
    >
        •
    </Box>
);

const chargingStates = {
    charging: {text: 'Charging', color: "#77C100B5"},
    active: {text: "Active", color: "#27c2ff"},
    off: {text: 'Inactive', color: "#ff2c2c"}
}

export default function Stats() {
    const [chargingState, setChargingState] = useState(chargingStates.off);
    const [chartData, setChartData] = useState({
        series: [{
            name: 'Temperature',
            type: 'area',
            data: td,
        }],
        options: {
            chart: {
                zoom: {
                    type: 'x',
                    enabled: true,
                    autoScaleYaxis: true
                },
                toolbar: {
                    show: true,
                     autoSelected: 'zoom'
                },
                height: 'auto',
                dropShadow: {
                    enabled: false,
                },
                id: 'area-datetime',
            },
            plotOptions: {
                bar: {
                    columnWidth: "20%"
                }
            },
            dataLabels: {
                enabled: false
            },
            markers: {
                hover: {
                    sizeOffset: 4
                }
            },
            // title: {
            //     text: 'Average Consumption',
            //     align: 'left'
            // },
            axisTicks: {
                show: true
            },
            axisBorder: {
                show: true,
            },
            yaxis: {
                title: {
                    text: '\u00D8 kWh/100km'
                },
                labels: {
                    formatter: function (value) {
                        return value.toFixed(0); // format to 0 decimal places
                    },
                },
            },
            xaxis: {
                type: 'datetime',
            },
            fill: {
                type: 'gradient',
                gradient: {
                    shadeIntensity: 1,
                    opacityFrom: 0.7,
                    opacityTo: 0.9,
                    stops: [0, 100]
                }
            },
            tooltip: {
                x: {
                    format: 'dd MMM yyyy'
                }
            },
            stroke: {
                curve: 'smooth'
            },
        },
    });

    return (
        <Box display={"grid"} gridTemplateColumns="repeat(12, 1fr)" gap={2} sx={{paddingX: 2}}>
            <Box sx={{gridColumn: {xs: "span 12", sm: "span 4"}, display: "flex", flexDirection: "column"}}>
                <Box sx={{
                    background: chargingState.color,
                    borderRadius: 1,
                    p: 3,
                    paddingY: 1,
                    display: "flex",
                    flexDirection: "column"
                }}>
                    <Typography component={"span"} sx={{textAlign: 'center'}}>{chargingState.text}</Typography>
                </Box>
                <Typography component={"span"} sx={{m: "0 0 1rem 0", fontWeight: "600"}}>Info</Typography>
                <Typography component={"span"}>Ladeleistung: 6kW</Typography>
                <Typography component={"span"} sx={{fontWeight: "600"}}>Verbrauch</Typography>
                <Typography component={"span"} sx={{textAlign: "right"}}>&#216; 10,9 kWh/100km</Typography>
                <Typography component={"span"} sx={{textAlign: "right"}}>{"17 kWh "}{bull}{" d"}</Typography>
                <Typography component={"span"} sx={{textAlign: "right"}}>{"170 kWh "}{bull}{" a"}</Typography>
            </Box>
            <Box sx={{gridColumn: {xs: "span 12", sm: "span 8"}}}>
                {/*<ReactApexChart*/}
                {/*    options={chartData.options}*/}
                {/*    series={chartData.series}*/}
                {/*/>*/}
            </Box>
        </Box>
    );
}