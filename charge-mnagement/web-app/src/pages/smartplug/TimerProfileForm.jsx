import Box from "@mui/material/Box";
import {Form, Formik} from "formik";
import Typography from "@mui/material/Typography";
import {
    FormControl,
    Input, InputAdornment, InputLabel, Select,
    Switch, useTheme
} from "@mui/material";
import {EvStationOutlined, OutletOutlined} from "@mui/icons-material";
import {LocalizationProvider, StaticTimePicker} from "@mui/x-date-pickers";
import {AdapterDayjs} from "@mui/x-date-pickers/AdapterDayjs";
import Button from "@mui/material/Button";
import {useState} from "react";
import dayjs from "dayjs";
import utc from "dayjs/plugin/utc";
import timezone from "dayjs/plugin/timezone";
import MenuItem from "@mui/material/MenuItem";
import {WeekdayPicker} from "../../components/component";

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.guess();

const plugs = [
    {id: 0, name: "tasmota-01", friendlyName: "tasmota-01"},
    {id: 1, name: "tasmota-02", friendlyName: "tasmota-02"},
    {id: 2, name: "tasmota-03", friendlyName: "tasmota-03"},
    {id: 3, name: "tasmota-04", friendlyName: "tasmota-04"},
    {id: 4, name: "tasmota-05", friendlyName: "tasmota-05"},
    {id: 5, name: "tasmota-06", friendlyName: "schlangen"},
]

const TimerProfileForm = ({close, profile, onSubmit}) => {
    const [time, setTime] = useState(dayjs.utc(profile.time) || dayjs("2023-05-05T07:30:00Z"));
    const [duration, setDuration] = useState(dayjs.utc(profile.duration) || dayjs("2023-08-08T00:00:00Z"));
    const theme = useTheme();

    const handleSubmit = (values) => {
        onSubmit(values);
    }

    return (
        <Box
            sx={{
                display: "flex",
                flexDirection: "column",
            }}
        >
            <Formik onSubmit={(values, actions) => handleSubmit(values, actions)}
                    initialValues={profile}
            >
                {({
                      handleSubmit,
                      handleChange,
                      handleBlur,
                      values,
                      touched,
                      isValid,
                      errors,
                  }) => (
                    <Form onSubmit={handleSubmit}>
                        <Box sx={{display: 'flex', gap: 2, flexDirection: 'column'}}>
                            <Box sx={{
                                display: 'flex',
                                gap: 2,
                                flexWrap: 'wrap',
                                flexDirection: {xs: 'column', sm: 'row'}
                            }}>

                                {/* duration picker */}
                                <Box sx={{flexGrow: 1,}}>
                                    <LocalizationProvider
                                        dateAdapter={AdapterDayjs}
                                    >
                                        <StaticTimePicker defaultValue={dayjs('2023-08-08T00:01')}
                                                          value={duration}
                                                          onChange={(e) => setDuration(e)}
                                                          sx={{
                                                              background: theme.palette.bg.main,
                                                              borderRadius: 2,
                                                              height: '100%'
                                                          }}
                                                          localeText={{
                                                              todayButtonLabel: "NOW",
                                                              timePickerToolbarTitle: "duration"
                                                          }}
                                                          slotProps={{actionBar: {actions: []},}}
                                                          ampm={false}/>
                                    </LocalizationProvider>
                                </Box>

                                {/* weekday picker */}
                                <Box sx={{
                                    background: theme.palette.bg.main,
                                    borderRadius: 2,
                                    p: 2,
                                    flexGrow: 1,
                                    display: 'flex',
                                    flexDirection: 'column',
                                    justifyContent: 'center',
                                    width: 'max-available'
                                }}>
                                    <WeekdayPicker checkedDays={values.days} onChange={handleChange} name={'days'}/>
                                </Box>

                                <Box sx={{
                                    display: 'flex',
                                    gap: 2,
                                    flexDirection: 'column',
                                    background: theme.palette.bg.main,
                                    borderRadius: 2,
                                    p: 2,
                                    flexGrow: 1
                                }}>

                                    {/* charge to SoC */}
                                    <FormControl fullWidth
                                                 onBlur={handleBlur}
                                                 error={touched.chargeToSoC && !!errors.chargeToSoC}
                                                 size={"small"}>
                                        <InputLabel>Charge to SoC</InputLabel>
                                        <Input
                                            // inputProps={{readOnly: true, disableUnderline: true}}
                                            type={"number"}
                                            id={"chargeToSoC"}
                                            endAdornment={<InputAdornment position="end">%</InputAdornment>}
                                            value={values.chargeToSoC}
                                            onChange={handleChange}
                                        />
                                    </FormControl>

                                    {/* current SoC */}
                                    <FormControl fullWidth
                                                 onBlur={handleBlur}
                                                 error={touched.soc && !!errors.soc}
                                                 size={"small"}>
                                        <InputLabel>current SoC</InputLabel>
                                        <Input
                                            // inputProps={{readOnly: true, disableUnderline: true}}
                                            type={"number"}
                                            id={"soc"}
                                            endAdornment={<InputAdornment position="end">%</InputAdornment>}
                                            value={values.soc}
                                            onChange={handleChange}
                                        />
                                    </FormControl>

                                    {/* plug */}
                                    <FormControl fullWidth
                                                 variant={'standard'}
                                                 onBlur={handleBlur}
                                                 error={touched.plugAddress && !!errors.plugAddress}
                                                 size={"small"}>
                                        <InputLabel>smart plug</InputLabel>
                                        <Select
                                            id={"plugAddress"}
                                            name="plugAddress"
                                            value={values.plugAddress}
                                            endAdornment={
                                                <InputAdornment position="end">
                                                    <OutletOutlined sx={{color: theme.palette.text.secondary}}/>
                                                </InputAdornment>}
                                            onChange={handleChange}
                                            IconComponent={() => null}
                                        >
                                            {plugs.map((plug) => {
                                                return (
                                                    <MenuItem value={plug.id}
                                                              id={plug.name + plug.id}
                                                              key={plug.id}>
                                                        {plug.name}
                                                    </MenuItem>
                                                )
                                            })}
                                        </Select>
                                    </FormControl>

                                    {/* charge */}
                                    <FormControl fullWidth
                                                 onBlur={handleBlur}
                                                 error={touched.charge && !!errors.charge}
                                                 size={"small"}>
                                        <InputLabel>charge to</InputLabel>
                                        <Input
                                            // inputProps={{readOnly: true, disableUnderline: true}}
                                            type={"number"}
                                            id={"charge"}
                                            endAdornment={<InputAdornment position="end">W</InputAdornment>}
                                            value={values.charge}
                                            onChange={handleChange}
                                        />
                                    </FormControl>

                                    {/* full charge */}
                                    <FormControl fullWidth
                                                 onBlur={handleBlur}
                                                 error={touched.fullCharge && !!errors.fullCharge}
                                                 size={"small"}>
                                        <InputLabel>Full Charge</InputLabel>
                                        <Input
                                            // inputProps={{readOnly: true, disableUnderline: true}}
                                            type={"number"}
                                            id={"fullCharge"}
                                            endAdornment={<InputAdornment position="end">W</InputAdornment>}
                                            value={values.fullCharge}
                                            onChange={handleChange}
                                        />
                                    </FormControl>

                                    {/* name */}
                                    <FormControl fullWidth
                                                 onBlur={handleBlur}
                                                 error={touched.name && !!errors.name}
                                                 size={"small"}>
                                        <InputLabel>Name</InputLabel>
                                        <Input
                                            // inputProps={{readOnly: true, disableUnderline: true}}
                                            type={"text"}
                                            id={"name"}
                                            value={values.name}
                                            onChange={handleChange}
                                        />
                                    </FormControl>
                                </Box>

                                {/* start on time */}
                                <Box sx={{background: theme.palette.bg.main, borderRadius: 2, flexGrow: 1}}>
                                    <Box sx={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
                                        <Typography sx={{textTransform: 'uppercase', fontSize: '.75rem', px: 2}}>
                                            Timed Start
                                        </Typography>
                                        <Switch
                                            // inputProps={{readOnly: true, disableUnderline: true}}
                                            id={"startOnTime"}
                                            value={values.startOnTime}
                                            onChange={handleChange}
                                        />
                                    </Box>
                                    <LocalizationProvider
                                        dateAdapter={AdapterDayjs}
                                        localeText={{clearButtonLabel: 'Empty', todayButtonLabel: 'Now'}}
                                    >
                                        <StaticTimePicker defaultValue={dayjs('2023-08-08T00:01')}
                                                          value={time}
                                                          onChange={(e) => setTime(e)}
                                                          sx={{background: 'none', borderRadius: 2, mb: 2}}
                                                          localeText={{
                                                              todayButtonLabel: "NOW",
                                                              timePickerToolbarTitle: "select start"
                                                          }}
                                                          slotProps={{
                                                              actionBar: {actions: ["today"]},
                                                              toolbar: {'&.MuiTimePickerToolbar-ampmLabel': {color: "green"}}
                                                          }}
                                                          ampm={false}/>
                                    </LocalizationProvider>
                                </Box>


                            </Box>
                            <Box sx={{display: 'flex', gap: 2, px: 2, pb: 2}}>
                                <Button variant={"outlined"} fullWidth onClick={close}>cancel</Button>
                                <Button type={"submit"} variant={"contained"} color={"primary"}
                                        fullWidth>save</Button>
                            </Box>
                        </Box>
                    </Form>
                )}
            </Formik>
        </Box>
    )
}

export default TimerProfileForm;