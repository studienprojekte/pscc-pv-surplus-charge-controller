import {Box, Dialog, Grid, Slide, Typography, useMediaQuery, useTheme} from '@mui/material';
import TimerProfile from "./TimerProfile";
import {forwardRef, useState} from "react";
import Button from "@mui/material/Button";
import {AddOutlined, MoreVertOutlined} from "@mui/icons-material";
import TimerProfileForm from "./TimerProfileForm";
import IconButton from "@mui/material/IconButton";

// chargingType 0 -> duration
// chargingType 1 -> charge to 'chargeToSoC'
// chargingType 2 -> charge to 'charge'

const profiles = [
    {
        id: 1,
        name: "E-Bike",
        active: true,
        days: [1, 3,],
        time: new Date("2023-05-05T05:30:00Z").toISOString(),
        duration: new Date("2023-08-08T00:00:00Z").toISOString(),
        chargeToSoC: 70,
        soc: 20,
        fullCharge: 1000,
        charge: 700,
        startOnTime: false,
        plugAddress: 3,
        chargingType: 0,
    },
    {
        id: 2,
        name: "bedampfen",
        active: true,
        days: [],
        time: new Date("2023-05-05T05:30:00Z").toISOString(),
        duration: new Date("2023-08-08T00:00:00Z").toISOString(),
        chargeToSoC: 70,
        soc: 0,
        fullCharge: 0,
        charge: 0,
        startOnTime: true,
        plugAddress: 5,
        chargingType: 1,
    },
    {
        id: 1,
        name: "E-Bike 2",
        active: true,
        days: [1, 3,],
        time: new Date("2023-05-05T05:30:00Z").toISOString(),
        duration: new Date("2023-08-08T00:00:00Z").toISOString(),
        chargeToSoC: 70,
        soc: 20,
        fullCharge: 1000,
        charge: 700,
        startOnTime: false,
        plugAddress: 3,
        chargingType: 2,
    },
    {
        id: 2,
        name: "bedampfen 2",
        active: true,
        days: [],
        time: new Date("2023-05-05T05:30:00Z").toISOString(),
        duration: new Date("2023-08-08T00:00:00Z").toISOString(),
        chargeToSoC: 70,
        soc: 0,
        fullCharge: 0,
        charge: 0,
        startOnTime: false,
        plugAddress: 5,
        chargingType: 1,
    }
]

const defaultProfile = {
    id: 0,
    name: "",
    active: false,
    days: [],
    time: new Date(Date.now()).toISOString(),
    duration: new Date("2023-05-05T00:00:00Z").toISOString(),
    chargeToSoC: 0,
    soc: 0,
    fullCharge: 0,
    charge: 0,
    startOnTime: false,
    plugAddress: 0,
    chargingType: 0,
}

const Transition = forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props}/>;
});

const PlugTimers = () => {
    const theme = useTheme();
    const [open, setOpen] = useState(false);
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
    const [timerProfiles, setTimerProfiles] = useState(profiles);

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const onSave = () => {
        // TODO
        setTimerProfiles(timerProfiles);
    }

    const handleAddTimer = (values) => {
        // TODO
        console.log(values);
        console.log('Timer added');
        setOpen(false);
    }

    const AddDialog = () => {
        return (
            <Dialog
                fullScreen={fullScreen}
                fullWidth={true}
                maxWidth={"xl"}
                open={open}
                onClose={handleClose}
                TransitionComponent={Transition}
                PaperProps={{
                    style: {background: theme.palette.bg.dark}, // Hintergrundfarbe setzen
                }}
            >
                <Box sx={{display: 'flex', flexDirection: 'column', gap: 2}}>
                    <Typography
                        sx={{
                            px: 2,
                            pt: 2,
                            fontWeight: 700,
                            color: theme.palette.bg.contrastText,
                            textTransform: 'uppercase'
                        }}>
                        add timer profile
                    </Typography>
                    <TimerProfileForm profile={defaultProfile} close={handleClose} onSubmit={handleAddTimer}/>
                </Box>
            </Dialog>
        )
    }

    return (
        <Box sx={{display: 'grid', gap: 2}}>

            {/* action Bar */}
            <Box sx={{mx: 2, display: 'flex', justifyContent: 'space-between'}}>
                {/* action Bar label */}
                <Typography
                    sx={{
                        color: theme.palette.secondary.contrastText,
                        textTransform: 'uppercase'
                    }}>
                    Timer
                </Typography>

                {/* action Bar actions */}
                <Box>
                    <IconButton onClick={handleOpen}>
                        <AddOutlined/>
                        <AddDialog/>
                    </IconButton>
                    {/* TODO: sort profiles -> custom, alpha ... */}
                    <IconButton onClick={handleOpen} disabled>
                        <MoreVertOutlined/>
                    </IconButton>
                </Box>
            </Box>

            {/* Timer Profiles Grid */}
            <Grid container
                  sx={{px: {xs: 0, sm: 2}}}
                  rowSpacing={{xs: 2, sm: 2}}
                  columnSpacing={{xs: 2, sm: 2}}>
                {timerProfiles?.map((profile) => {
                    return (
                        <Grid item xs={12} md={6} lg={4} key={"timer-profile-" + profile.name}>
                            <TimerProfile profile={profile}/>
                        </Grid>
                    )
                })}

                {/* add profile button */}
                {Object.keys(timerProfiles).length === 0 &&
                    <Grid item xs={12} md={6} lg={4} key={"timer-profile-add"}>
                        <Box
                            sx={{
                                background: theme.palette.bg.main,
                                borderRadius: 2,
                                width: '100%',
                                height: '100%'
                            }}>
                            <Button sx={{width: '100%', height: '100%'}} onClick={handleOpen}>
                                <AddOutlined/>add
                            </Button>
                            <AddDialog/>
                        </Box>
                    </Grid>
                }
            </Grid>
        </Box>
    )
}

export default PlugTimers;
