import {Box, Grid, Typography, useTheme} from '@mui/material';

const CustomPlugActions = () =>  {
    const theme = useTheme();
    return (
        <Box sx={{display: "grid", flexDirection: "column", gap: 2, px: {xs: 1, sm: 2} }}>
            {/*<Box sx={{display: "flex", justifyContent: "space-between"}}>*/}
            {/*    <Typography*/}
            {/*        component={"span"}*/}
            {/*        sx={{paddingX: 3, fontWeight: "600", color: theme.palette.bg.contrastText, textTransform: 'uppercase'}}>*/}
            {/*        Charge Now*/}
            {/*    </Typography>*/}
            {/*</Box>*/}
            <Grid container
                  rowSpacing={{ xs: 1, sm: 2}}
                  columnSpacing={{ xs: 1, sm: 2}}>
                <Grid item xs={6} sm={4} md={3}>
                    <Box sx={{background: theme.palette.secondary.main, p: '1rem', borderRadius: '.5rem'}}>
                        <Typography
                            sx={{
                                color: theme.palette.secondary.contrastText,
                                textTransform: 'uppercase',
                                fontWeight: 400,
                                fontSize: '2rem'}}>
                            action
                        </Typography>
                    </Box>
                </Grid>
                <Grid item xs={6} sm={4} md={3}>
                    <Box sx={{background: theme.palette.warning.dark, p: '1rem', borderRadius: '.5rem'}}>
                        <Typography
                            sx={{
                                color: theme.palette.secondary.contrastText,
                                textTransform: 'uppercase',
                                fontWeight: 400,
                                fontSize: '2rem'}}>
                            action
                        </Typography>
                    </Box>
                </Grid>
                <Grid item xs={6} sm={4} md={3}>
                    <Box sx={{background: theme.palette.success.dark, p: '1rem', borderRadius: '.5rem'}}>
                        <Typography
                            sx={{
                                color: theme.palette.secondary.contrastText,
                                textTransform: 'uppercase',
                                fontWeight: 400,
                                fontSize: '2rem'}}>
                            action
                        </Typography>
                    </Box>
                </Grid>
                <Grid item xs={6} sm={4} md={3}>
                    <Box sx={{background: theme.palette.primary.light, p: '1rem', borderRadius: '.5rem'}}>
                        <Typography
                            sx={{
                                color: theme.palette.secondary.contrastText,
                                textTransform: 'uppercase',
                                fontWeight: 400,
                                fontSize: '2rem'}}>
                            action
                        </Typography>
                    </Box>
                </Grid>
            </Grid>
        </Box>
    )
}

export default CustomPlugActions;