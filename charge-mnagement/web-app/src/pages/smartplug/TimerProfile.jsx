import {Box, Dialog, Slide, Switch, Typography, useMediaQuery, useTheme, Button} from "@mui/material";
import {forwardRef, useState} from "react";
import TimerProfileForm from "./TimerProfileForm";
import {WeekdayDisplay} from "../../components/component";

const Transition = forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props}/>;
});

const TimerProfile = ({profile}) => {
    const theme = useTheme();
    const [active, setActive] = useState(profile?.startOnTime);
    const [open, setOpen] = useState(false);
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleSubmit = (values) => {
        // TODO
        console.log(values);
        console.log('Änderungen gespeichert');
        setOpen(false);
    };

    const handleStart = (e) => {
        // TODO
        e.stopPropagation();
        console.log('started');
    }

    function formaterTime(isoString, utc= false) {
        const date = new Date(isoString);
        const hours = String(utc ? date.getUTCHours() : date.getHours()).padStart(2, '0');
        const minutes = String(utc ? date.getUTCMinutes() : date.getMinutes()).padStart(2, '0');

        return `${hours}:${minutes}`;
    }

    function isEmptyArray(array) {
        return Array.isArray(array) && array.length === 0;
    }

    return (
        <>
            <Box
                onClick={handleOpen}
                sx={{
                    background: active ? theme.palette.success.dark : theme.palette.bg.main,
                    transitionTimingFunction: 'cubic-bezier(0.25, 0.1, 0.25, 1)',
                    transition: '.2s',
                    p: 2,
                    display: "grid",
                    gap: 1,
                    borderRadius: 2,
                    cursor: 'pointer'
                }}>
                <Typography sx={{textTransform: 'uppercase', color: theme.palette.bg.contrastText}}>
                    {profile?.name}
                </Typography>
                <Box
                    sx={{
                        display: "flex",
                        flexWrap: 'wrap',
                        gap: 2,
                        justifyContent: "space-between"
                    }}>
                    <Box sx={{display: "flex", alignItems: 'center', justifyContent: 'center', gap: 2}}>
                        <Box sx={{display: profile?.startOnTime ? 'block' : 'none'}}>
                            <Typography
                                sx={{
                                    color: theme.palette.text.secondary,
                                    textTransform: 'uppercase',
                                    fontWeight: 400,
                                    fontSize: '.5rem'
                                }}>
                                start
                            </Typography>
                            <Typography
                                sx={{
                                    color: theme.palette.secondary.contrastText,
                                    textTransform: 'uppercase',
                                    fontWeight: 400,
                                    fontSize: '2rem'
                                }}>
                                {formaterTime(profile.time)}
                            </Typography>
                        </Box>
                        <Box sx={{display: profile?.chargingType === 0 ? 'block' : 'none'}}>
                            <Typography
                                sx={{
                                    color: theme.palette.text.secondary,
                                    textTransform: 'uppercase',
                                    fontWeight: 400,
                                    fontSize: '.5rem'
                                }}>
                                duration
                            </Typography>
                            <Typography
                                sx={{
                                    color: theme.palette.secondary.contrastText,
                                    textTransform: 'uppercase',
                                    fontWeight: 400,
                                    fontSize: '2rem'
                                }}>
                                {formaterTime(profile.duration, true)}
                            </Typography>
                        </Box>

                        {/* charge to SoC */}
                        <Box sx={{display: profile?.chargingType === 1 ? 'block' : 'none'}}>
                            <Typography
                                sx={{
                                    color: theme.palette.text.secondary,
                                    textTransform: 'uppercase',
                                    fontWeight: 400,
                                    fontSize: '.5rem'
                                }}>
                                charge mode
                            </Typography>
                            <Typography
                                sx={{
                                    color: theme.palette.secondary.contrastText,
                                    textTransform: 'uppercase',
                                    fontWeight: 400,
                                    fontSize: '2rem'
                                }}>
                                {profile?.chargeToSoC} %
                            </Typography>
                        </Box>

                        {/* charge specified power */}
                        <Box sx={{display: profile?.chargingType === 2 ? 'block' : 'none'}}>
                            <Typography
                                sx={{
                                    color: theme.palette.text.secondary,
                                    textTransform: 'uppercase',
                                    fontWeight: 400,
                                    fontSize: '.5rem'
                                }}>
                                charge mode
                            </Typography>
                            <Typography
                                sx={{
                                    color: theme.palette.secondary.contrastText,
                                    textTransform: 'uppercase',
                                    fontWeight: 400,
                                    fontSize: '2rem'
                                }}>
                                {profile?.charge} w
                            </Typography>
                        </Box>
                    </Box>

                    {/* actions */}
                    <Box sx={{display: "flex", alignItems: 'center', justifyContent: 'end', gap: 2, flexGrow: 1}}>
                        {!isEmptyArray(profile?.days) &&
                            <WeekdayDisplay checkedDays={profile?.days}/>
                        }
                        {profile?.startOnTime || !isEmptyArray(profile?.days) ?
                            <Switch value={100}
                                    sx={{m: 0}}
                                    checked={!!active}
                                    defaultValue={profile?.startOnTime}
                                    onClick={(e) => e.stopPropagation()}
                                    onChange={() => setActive(!active)}/>
                            :
                            <Button onClick={handleStart} color={'success'} variant={'outlined'}>
                                start
                            </Button>
                        }

                    </Box>
                </Box>
            </Box>
            <Dialog
                fullScreen={fullScreen}
                fullWidth={true}
                maxWidth={"xl"}
                open={open}
                onClose={handleClose}
                TransitionComponent={Transition}
                PaperProps={{
                    style: {background: theme.palette.bg.dark}, // Hintergrundfarbe setzen
                }}
            >
                <Box sx={{display: 'flex', flexDirection: 'column', gap: 2}}>
                    <Typography
                        sx={{
                            px: 2,
                            pt: 2,
                            fontWeight: 700,
                            color: theme.palette.bg.contrastText,
                            textTransform: 'uppercase'
                        }}>
                        {profile.name}
                    </Typography>
                    <TimerProfileForm profile={profile} close={handleClose} onSubmit={handleSubmit}/>
                </Box>
            </Dialog>
        </>
    )
}

export default TimerProfile;