import CustomPlugActions from "./CustomPlugActios";
import PlugTimers from "./PlugTimers";
import Box from "@mui/material/Box";


function SmartPlug() {
    return (
        <>
            <Box sx={{display: 'flex', gap: 3, flexDirection: 'column'}}>
                <CustomPlugActions/>
                <PlugTimers/>
            </Box>
        </>
    )
}

export default SmartPlug;
