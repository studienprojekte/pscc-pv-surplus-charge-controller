import { Box } from '@mui/material';

const Dashboard = () =>  {
    return (
        <Box sx={{display: 'grid', backgroundColor: '#dfecf5', gap: 3}}>
            <Box sx={{display: 'flex', backgroundColor: '#fff'}}>
                Dashboard Card
            </Box>
            <Box sx={{display: 'flex', backgroundColor: '#fff'}}>
                Dashboard Card
            </Box>
        </Box>
    )
}

export default Dashboard
