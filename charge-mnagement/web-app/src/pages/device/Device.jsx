import {
    Box, FormControl, Grid, Input, InputAdornment, InputLabel, Typography, useTheme
} from "@mui/material";
import React, {useEffect, useState} from "react";
import TimerProfile from "../smartplug/TimerProfile.jsx";
import Button from "@mui/material/Button";
import {AddOutlined, MoreVertOutlined} from "@mui/icons-material";
import IconButton from "@mui/material/IconButton";

let filters = [
    "wallbox", "vehicle", "smartplug", "heat"
]

function Device() {
    let theme = useTheme()
    const [devices, setDevices] = useState([])

    useEffect(() => {
        // Function to fetch data from the API
        const fetchData = async () => {
            try {
                const response = await fetch('http://127.0.0.1:5000/device');
                const result = await response.json();

                // Update the state with the fetched data
                setDevices(result);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };

        // Call the fetchData function when the component mounts
        fetchData();
    }, []); // Empty dependency array ensures the effect runs only once on mount


    let get_filters = () => {
        return (
            filters.map(item => {
                return (
                    <Grid item xs={6} sm={4} md={3} key={"device-filter-action-" + item}>
                        <Box sx={{background: theme.palette.secondary.main, p: '1rem', borderRadius: '.5rem'}}>
                            <Typography
                                sx={{
                                    color: theme.palette.secondary.contrastText,
                                    textTransform: 'uppercase',
                                    fontWeight: 400,
                                    fontSize: '2rem'
                                }}>
                                {item}
                            </Typography>
                        </Box>
                    </Grid>
                )
            })
        )
    }

    let get_devices = () => {
        return (
            <Grid container
                  sx={{px: {xs: 0, sm: 2}}}
                  rowSpacing={{xs: 2, sm: 2}}
                  columnSpacing={{xs: 2, sm: 2}}>
                {devices?.map((dev) => {
                    return (
                        <Grid item xs={12} md={6} lg={4} key={"timer-dev-" + dev.name}>
                            <Box
                                sx={{
                                    background: theme.palette.bg.main,
                                    transitionTimingFunction: 'cubic-bezier(0.25, 0.1, 0.25, 1)',
                                    transition: '.2s',
                                    p: 2,
                                    display: "grid",
                                    gap: 1,
                                    borderRadius: 2,
                                    cursor: 'pointer'
                                }}>
                                <Typography sx={{textTransform: 'uppercase', color: theme.palette.bg.contrastText}}>
                                    {dev?.name}
                                </Typography>
                                <Box sx={{
                                    display: 'flex',
                                    gap: 2,
                                    flexDirection: 'column',
                                    background: theme.palette.bg.main,
                                    borderRadius: 2,
                                    p: 2,
                                    flexGrow: 1
                                }}>
                                    {dev?.data?.map(d => {
                                        return (
                                            <FormControl fullWidth
                                                         size={"small"}
                                                         key={dev?.name + d?.name}>
                                                <InputLabel>{d?.name}</InputLabel>
                                                <Input
                                                    // inputProps={{readOnly: true, disableUnderline: true}}
                                                    type={"text"}
                                                    id={d?.name}
                                                    value={d?.value}
                                                    disabled
                                                />
                                            </FormControl>
                                        )
                                    })}
                                </Box>
                            </Box>
                        </Grid>
                    )
                })}

                {/* add dev button */}
                {Object.keys(devices).length === 0 &&
                    <Grid item xs={12} md={6} lg={4} key={"device-add"}>
                        <Box
                            sx={{
                                background: theme.palette.bg.main,
                                borderRadius: 2,
                                width: '100%',
                                height: '100%'
                            }}>
                            {/*<Button sx={{width: '100%', height: '100%'}} onClick={handleOpen}>*/}
                            {/*    <AddOutlined/>add*/}
                            {/*</Button>*/}
                            {/*<AddDialog/>*/}
                        </Box>
                    </Grid>
                }
            </Grid>
        )
    }

    return (
        <>
            <Box sx={{display: 'flex', gap: 3, flexDirection: 'column'}}>
                <Box sx={{display: "grid", flexDirection: "column", gap: 2, px: {xs: 1, sm: 2}}}>
                    <Grid container
                          rowSpacing={{xs: 1, sm: 2}}
                          columnSpacing={{xs: 1, sm: 2}}
                          key={"device-filter-actions"}>
                        {get_filters()}
                    </Grid>
                </Box>
                <Box sx={{display: 'grid', gap: 2}}>

                    {/* action Bar */}
                    <Box sx={{mx: 2, display: 'flex', justifyContent: 'space-between'}}>
                        {/* action Bar label */}
                        <Typography
                            sx={{
                                color: theme.palette.secondary.contrastText,
                                textTransform: 'uppercase'
                            }}>
                            Devices
                        </Typography>

                        {/* action Bar actions */}
                        <Box>
                            <IconButton disabled>
                                <AddOutlined/>
                                {/*<AddDialog/>*/}
                            </IconButton>
                            {/* TODO: sort profiles -> custom, alpha ... */}
                            <IconButton disabled>
                                <MoreVertOutlined/>
                            </IconButton>
                        </Box>
                    </Box>
                    {get_devices()}
                </Box>
            </Box>
        </>
    )
}

export default Device;
