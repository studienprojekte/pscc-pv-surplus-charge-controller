export {default as Dashboard} from "./index/Dashboard";
export {default as Charger} from "./charger/Charger";
export {default as About} from "./about/About";
export {default as SmartPlug} from "./smartplug/SmartPlug";
export {default as Device} from "./device/Device.jsx";
