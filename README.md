The presented work introduces the implementation of dynamic load management for batteryelectric Vehicles.
The realization is achieved through the integration of microservices covering various aspects of load management.

A central element involves collecting data on certain parameters of BEVs using an ESP32-based CAN dongle.
This dongle enables access to the State of Charge (SOC) of the vehicle battery via the On-Board-Diagnostic (OBD) interface.

The development of the load management system follows the microservices architectural style, facilitating efficient organization and control of different tasks within load management.
Some performance data, such as PV or grid consumption, are captured by microservices and processed by others.
Another microservice handles data storage for settings and measurement data time series in a database, ensuring efficient management and analysis.

The integration of interface adapters to MQTT as an exchange medium enables reliable communication between microservices and other system components.
A dedicated monitoring and configuration microservice provides monitoring functions to oversee system performance, identify potential issues early, and allows for the adjustment of the charge controller through charging profiles.

At the core of the load management is a charge controller, which ensures efficient control of the BEV battery charging process based on the collected data.

This work not only addresses challenges in the field of BEV load management but also emphasizes the benefits of a holistic approach, providing an innovative contribution to solving the future vision of a Smart Grid.
