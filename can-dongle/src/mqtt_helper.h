#pragma once

#include <mqtt_client.h>

static esp_mqtt_client_handle_t MqttClient;

void pub_to(String topic, String data);

void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data);

void mqtt_start(void);
