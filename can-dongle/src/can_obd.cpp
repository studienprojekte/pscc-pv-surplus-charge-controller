#include <Arduino.h>

#include "can_obd.h"
#include "config.h"
#include "mqtt_helper.h"

#include "driver/gpio.h"
#include "driver/twai.h"
#include <mqtt_client.h>
#include <vector>
#include <sstream>

/**
 * @brief   vehicle poll list for vw e-up
 */
const poll_pid_t vweup_polls[] = {
    {VWUP_CHG_MGMT, UDS_READ, VWUP_CHG_MGMT_SOC_NORM,         {  0, 20, 20, 20}, 1, ISOTP_STD},
    {VWUP_BAT_MGMT, UDS_READ, VWUP_BAT_MGMT_SOC_ABS,          {  0, 20, 20, 20}, 1, ISOTP_STD},
    {VWUP_CHG_MGMT, UDS_READ, VWUP_CHG_MGMT_SOC_LIMITS,       {  0, 12, 12, 12}, 1, ISOTP_STD},
};

/**
 * @brief   create a twai/can message from poll entry
 * 
 * @param msg
 * @param message
 */
void create_obd_message (poll_pid_t msg, twai_message_t *message){ // }, const uint8_t *data){
    message->identifier = msg.txmoduleid;
    message->rtr = 0;
    message->extd = 0;
    message->data_length_code = 8;
    message->data[0] = 0x03;
    message->data[1] = msg.type;
    message->data[2] = (uint8_t)(msg.pid >> 8);
    message->data[3] = (uint8_t)msg.pid;
    message->data[4] = 0xAA;
    message->data[5] = 0xAA;
    message->data[6] = 0xAA;
    message->data[7] = 0xAA;
}

/**
 * @brief   initialice the twai driver on esp32
 */
void init_twai(){
    //Initialize configuration structures using macro initializers
    twai_general_config_t g_config = TWAI_GENERAL_CONFIG_DEFAULT((gpio_num_t)PIN_CAN_TX, (gpio_num_t)PIN_CAN_RX, TWAI_MODE_NORMAL);
    twai_timing_config_t t_config = TWAI_TIMING_CONFIG_500KBITS();
    twai_filter_config_t f_config = TWAI_FILTER_CONFIG_ACCEPT_ALL();

    //Install TWAI driver
    if (twai_driver_install(&g_config, &t_config, &f_config) == ESP_OK) {
        Serial.println("Driver installed");
    } else {
        Serial.println("Failed to install driver");
        return;
    }

    //Start TWAI driver
    if (twai_start() == ESP_OK) {
        Serial.println("Driver started");
    } else {
        Serial.println("Failed to start driver");
        return;
    }
}

/**
 * @brief   TWAI Task for continous frame reading
 * 
 * @param pvParameters
 */
void TWAITask(void *pvParameters){
    while(1){
        get_obd_frame(0x22);
        vTaskDelay(pdMS_TO_TICKS(100));
    }
}

/**
 * @brief   request a PID from list
 * 
 * @param msg   PID name
 */
void request_pid(can_obd_msg_t msg){

    // creating request message
    twai_message_t message;
    switch (msg)
    {
    case SOC:
        create_obd_message(vweup_polls[SOC], &message);
        break;
    case SOC_ABS:
        create_obd_message(vweup_polls[SOC_ABS], &message);
        break;
    case SOC_LIMIT:
        create_obd_message(vweup_polls[SOC_LIMIT], &message);
        break;
    default:
        break;
    }

    // request pid
    // Queue message for transmission
    Serial.print(message.identifier, HEX);
    Serial.print(" | ");
    for (int i = 0; i < 8; i++){
        if (message.data[i] < 16) Serial.print("0");
        Serial.print(message.data[i], HEX);
        if ((i + 1) % 2) Serial.print(" ");
    }
    Serial.print("\t-> message queued for transmission");
    if (twai_transmit(&message, pdMS_TO_TICKS(2500)) == ESP_OK) {
        Serial.println(" ACK");
    } else {
        Serial.println("\nFailed to queue message for transmission");
    }
}

/**
 * @brief   get curent time as UTC Iso String
 * 
 * @return  Time String
 */
String getUTCTimeISOstrNow(){
    time_t now;
    char strftime_buf[64];
    String timeString;
    struct tm timeinfo;

    time(&now);
    // Set timezone UTC Standard Time
    setenv("TZ", "CST+0", 1);
    tzset();

    localtime_r(&now, &timeinfo);
    strftime(strftime_buf, sizeof(strftime_buf), "%FT%TZ%z", &timeinfo);
    timeString = (String)strftime_buf;
    return timeString;
}

/**
 * @brief   TWAI receive Message and prozessing - publish to MQTT
 * 
 * @param type UDS request type
 */
int get_obd_frame(uint8_t type){
    std::stringstream printer;
    // printer << "\nWaiting for Message";

    digitalWrite(PIN_LED_BUILTIN, HIGH);

    twai_message_t message;

    //Wait for message to be received
    if (twai_receive(&message, pdMS_TO_TICKS(TWAI_TIMEOUT)) != ESP_OK) {
        Serial.println("Failed to receive message\n");
        return -1;
    }
    digitalWrite(PIN_LED_BUILTIN, LOW);

    //Process received message
    printer << (message.extd ? "EXT " : "") << "ID " << std::hex << message.identifier
            << (message.rtr ? "\tRTR" : " len: ") << static_cast<unsigned>(message.data_length_code) << std::endl;
    
    if (!(message.rtr) && message.data[1] - 0x40 == type) {
        printer << "data: 0x ";
        for (int i = 0; i < message.data_length_code; i++) {
            if (message.data[i] < 16) printer << "0";
            printer << std::hex << (int)message.data[i] << " ";
        }

        // extract PID from message
        uint16_t pid = (message.data[2] << 8 | message.data[3]);
        printer << "pid: " << std::hex << pid << std::endl;
        float val = 0;
        uint8_t min, max;

        // PID specific prozessing
        switch (pid)
        {
        case VWUP_CHG_MGMT_SOC_NORM:
            val = (float)message.data[4] / 2.f;
            pub_to("soc", (String)val);
            pub_to("soc/last_update", getUTCTimeISOstrNow());
            printer << "SoC: " << val << std::endl;
            break;
        case VWUP_BAT_MGMT_SOC_ABS:
            val = (float)message.data[4] / 2.56f;
            pub_to("soc_abs", (String)val);
            pub_to("soc_abs/last_update", getUTCTimeISOstrNow());
            printer << "SoC abs.: " << val << std::endl;
            break;
        case VWUP_CHG_MGMT_SOC_LIMITS:
            min = message.data[4];
            max = message.data[5];
            pub_to("limits/soc_min", (String)min);
            pub_to("limits/soc_max", (String)min);
            pub_to("limits/last_update", getUTCTimeISOstrNow());
            printer << "SoC limit " << std::dec << static_cast<unsigned>(min) << " - " << static_cast<unsigned>(max) << std::endl;
            break;
        default:
            break;
        }

        printer << std::endl;
    }

    Serial.print(printer.str().c_str());
    
    return 0;
}
