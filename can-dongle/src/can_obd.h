#pragma once

#include <hal/twai_types.h>

// OBD (ISO 15031) service identifiers supported:
#define VEHICLE_POLL_TYPE_OBDIICURRENT    0x01 // Mode 01 "current data" (8 bit PID)
#define VEHICLE_POLL_TYPE_OBDIIFREEZE     0x02 // Mode 02 "freeze frame data" (8 bit PID)
#define VEHICLE_POLL_TYPE_READ_ERDTC      0x03 // Mode 03 read emission-related DTC (no PID)
#define VEHICLE_POLL_TYPE_CLEAR_ERDTC     0x04 // Mode 04 clear/reset emission-related DTC (no PID)
#define VEHICLE_POLL_TYPE_READOXSTEST     0x05 // Mode 05 read oxygen sensor monitoring test results (16 bit PID)
#define VEHICLE_POLL_TYPE_READOBMTEST     0x06 // Mode 06 read on-board monitoring test results (8 bit PID)
#define VEHICLE_POLL_TYPE_READ_DCERDTC    0x07 // Mode 07 read driving cycle emission-related DTC (no PID)
#define VEHICLE_POLL_TYPE_REQOBUCTRL      0x08 // Mode 08 request on-board unit control (8 bit PID)
#define VEHICLE_POLL_TYPE_OBDIIVEHICLE    0x09 // Mode 09 "vehicle information" (8 bit PID)
#define VEHICLE_POLL_TYPE_READ_PERMDTC    0x0A // Mode 0A read permanent (cleared) DTC (no PID)

// UDS (ISO 14229) service identifiers supported:
#define VEHICLE_POLL_TYPE_OBDIISESSION    0x10 // UDS: Diagnostic Session Control (8 bit PID)
#define VEHICLE_POLL_TYPE_TESTERPRESENT   0x3E // UDS: TesterPresent (8 bit PID)
#define VEHICLE_POLL_TYPE_SECACCESS       0x27 // UDS: SecurityAccess (8 bit PID)
#define VEHICLE_POLL_TYPE_COMCONTROL      0x28 // UDS: CommunicationControl (8 bit PID)
#define VEHICLE_POLL_TYPE_ECURESET        0x11 // UDS: ECUReset (8 bit PID)
#define VEHICLE_POLL_TYPE_CLEARDTC        0x14 // UDS: ClearDiagnosticInformation (no PID)
#define VEHICLE_POLL_TYPE_READDTC         0x19 // UDS: ReadDTCInformation (8 bit PID)
#define VEHICLE_POLL_TYPE_OBDIIEXTENDED   0x22 // UDS: ReadDataByIdentifier (16 bit PID) (legacy alias for READDATA)
#define VEHICLE_POLL_TYPE_READDATA        0x22 // UDS: ReadDataByIdentifier (16 bit PID)
#define VEHICLE_POLL_TYPE_READMEMORY      0x23 // UDS: ReadMemoryByAddress (no PID)
#define VEHICLE_POLL_TYPE_READSCALING     0x24 // UDS: ReadScalingDataByIdentifier (16 bit PID)
#define VEHICLE_POLL_TYPE_WRITEDATA       0x2E // UDS: WriteDataByIdentifier (16 bit PID)
#define VEHICLE_POLL_TYPE_ROUTINECONTROL  0x31 // UDS: Routine Control (8 bit PID)
#define VEHICLE_POLL_TYPE_WRITEMEMORY     0x3D // UDS: WriteMemoryByAddress (8 bit PID)
#define VEHICLE_POLL_TYPE_IOCONTROL       0x2F // UDS: InputOutputControlByIdentifier (16 bit PID)

#define UDS_READ                        VEHICLE_POLL_TYPE_READDATA

//
// ECU TX/RX IDs                                                                          [Availability]
//
#define VWUP_CHG_MGMT_TX                0x765   // ECU BD HV charge management            [DRV,CHG]
#define VWUP_CHG_MGMT_RX                0x7CF

#define VWUP_BAT_MGMT_TX                0x7E5   // ECU 8C hybrid battery management       [DRV,CHG]
#define VWUP_BAT_MGMT_RX                0x7ED

#define VWUP_MOT_ELEC_TX                0x7E0   // ECU 01 motor elecronics                [DRV]
#define VWUP_MOT_ELEC_RX                0x7E8

//
// Poll list shortcuts                                                                    [Availability]
//
#define VWUP_CHG_MGMT                   VWUP_CHG_MGMT_TX, VWUP_CHG_MGMT_RX            //  [DRV,CHG]
#define VWUP_BAT_MGMT                   VWUP_BAT_MGMT_TX, VWUP_BAT_MGMT_RX            //  [DRV,CHG]
#define VWUP_MOT_ELEC                   VWUP_MOT_ELEC_TX, VWUP_MOT_ELEC_RX            //  [DRV]

//
// PIDs of ECUs
//

/**
 * @note    value = databyte[4]
 * @note    float soc = value / 2.56f
 */
#define VWUP_BAT_MGMT_SOC_ABS           0x028C

/**
 * @note    SoC matches the instrument cluster
 * @note    available while driving and while charging
 * @note    not linear ( does not compensate the voltage characteristics )
 * @note    shows calibration point(s) during charging -> can run backwards during a charge.
 * @note    value = databyte[4]
 * @note    val = float soc = value / 2.f
 */
#define VWUP_CHG_MGMT_SOC_NORM          0x1DD0

#define VWUP_CHG_MGMT_SOC_LIMITS        0x1DD1    // Minimum & maximum SOC

#define CMD_WakeupCar                   18        // ()

typedef enum {
    SOC = 0,
    SOC_ABS = 1,
    SOC_LIMIT = 2,
} can_obd_msg_t;

#define ISOTP_STD                       0     // standard addressing (11 bit IDs)
#define VEHICLE_POLL_NSTATES            4

typedef struct
{
    uint32_t txmoduleid;                      // transmission CAN ID (address), 0x7df = OBD2 broadcast
    uint32_t rxmoduleid;                      // expected response CAN ID or 0 for broadcasts
    uint16_t type;                            // UDS poll type / OBD2 "mode", see VEHICLE_POLL_TYPE_…
    union
    {
    uint16_t pid;                           // PID (shortcut for requests w/o payload)
    struct
        {
        uint16_t pid;                         // PID for requests with additional payload
        uint8_t datalen;                      // payload length (bytes)
        uint8_t data[6];                      // inline payload data (single frame request)
        } args;
    struct
        {
        uint16_t pid;                         // PID for requests with additional payload
        uint8_t tag;                          // needs to be POLL_TXDATA
        uint16_t datalen;                     // payload length (bytes, max 4095)
        const uint8_t* data;                  // pointer to payload data (single/multi frame request)
        } xargs;
    };
    uint16_t polltime[VEHICLE_POLL_NSTATES];  // poll intervals in seconds for used poll states
    uint8_t  pollbus;                         // 0 = default CAN bus from PollSetPidList(), 1…4 = specific
    uint8_t  protocol;                        // ISOTP_STD / ISOTP_EXTADR / ISOTP_EXTFRAME / VWTP_20
} poll_pid_t;

void init_twai();

void TWAITask(void *pvParameters);

void request_pid(can_obd_msg_t msg);

int get_obd_frame(uint8_t type);
