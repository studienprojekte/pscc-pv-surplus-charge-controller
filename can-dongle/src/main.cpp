/**
 * @author  jj
 * @date    18.11.23.
 */

#include <Arduino.h>
#include <WiFiMulti.h>

#include "main.h"
#include "config.h"
#include "mqtt_helper.h"
#include "can_obd.h"


WiFiMulti wifiMulti;

void init_time(){
    configTime(0, 0, NTP_SERVER_1, NTP_SERVER_2, NTP_SERVER_3);
}

void setup(){
    Serial.begin(9600);
    while (!Serial);
    Serial.println("\n" STARTUP_BANNER "");

    // Setup wifi
    WiFi.mode(WIFI_STA);
    wifiMulti.addAP(WIFI_SSID, WIFI_PASSWORD);

    // Connect to WIFI
    Serial.print("Connecting to wifi");
    while (wifiMulti.run() != WL_CONNECTED) {
        Serial.print(".");
        delay(200);
    }
    Serial.println();

    // initialize System Time - time from NTP Server
    init_time();

    // PIN Setup
    pinMode(PIN_LED_BUILTIN, OUTPUT);

    // initialize TWAI
    init_twai();
    
    // establish connecttion to MQTT Broker
    mqtt_start();
}


void loop()
{
    // Create a FreeRTOS task for TWAI processing
    xTaskCreatePinnedToCore(
        TWAITask,      // Task function
        "TWAITask",    // Task name
        8192,          // Stack size
        NULL,          // Task parameters
        1,             // Priority (1 is default)
        NULL,          // Task handle
        0              // Core to run the task (0 or 1)
    );

    while (1)
    {
        // request SoC from Charge Management (normalized)
        request_pid(SOC);
        delay(500);

        // request SoC from Battery Management (absolut)
        request_pid(SOC_ABS);
        delay(500);

        // request SoC charging limits from Charge Management
        request_pid(SOC_LIMIT);

        delay(POLLING_INTERVAL);
        Serial.println();
    }
    
}
