// #define REMOTE_DEV
#define DEBUG

#define DEVICE_UID "BLYA196E" //"8w7934oy"
#define DEVICE "ESP32"

// *** WIFI ***
#ifdef REMOTE_DEV
// WiFi SSID
#define WIFI_SSID "ssid"
// WiFi password
#define WIFI_PASSWORD "password"
#else
// WiFi SSID
#define WIFI_SSID "ssid"
// WiFi password
#define WIFI_PASSWORD "password"
#endif


// *** WIFI ACCESS POINT ***
// WiFi AP SSID
#define WIFI_AP_SSID "sc-car-ap"
// WiFi password
#define WIF_AP_PASSWORD "sc-car-ap"


/*
 * NTP Server
 */
#define NTP_SERVER_1 "pool.ntp.org"
#define NTP_SERVER_2 "de.pool.ntp.org"
#define NTP_SERVER_3 "pool.ntp.org"


// *** SERVER ***
// enables access through a web server
#define SERVER
#define SERVER_PORT 80
#define API_PORT 8080


// *** MQTT ***
#ifdef REMOTE_DEV
#define CONFIG_MQTT_URI "nextcloud.fritz.box"
#else
#define CONFIG_MQTT_HOST "smart-complete.fritz.box"
#endif
#define CONFIG_MQTT_PORT 1883
#define CONFIG_MQTT_PORT_SSL 8883
#define CONFIG_MQTT_SSL false
#define CONFIG_MQTT_V5 false
#define CONFIG_MQTT_BROKER_USERNAME "sensor"
#define CONFIG_MQTT_BROKER_PASSWORD "APMa1Lern3nFokus"

#define CONFIG_MQTT_CMND_ROOT "smarthome/cmnd/car"

#define CONFIG_MQTT_PUB_ROOT "smarthome/environment/car"

// *** INFLUX-DB SETTINGS ***
// InfluxDB v2 server url
#define INFLUXDB_URL "https://influx.jbyte.de"
// API token
#define INFLUXDB_TOKEN "_4LfsVbYAWhGXu3G4H7reUs4pVzPeow4w2lHPlGgrldGqSn6jzeqy5kG4zSMaM48Z2ks8tCgvCvqaUvD1pB5nQ=="
// InfluxDB v2 organization
#define INFLUXDB_ORG "jbyte"
// InfluxDB v2 bucket
#define INFLUXDB_BUCKET "jbyte_bucket"
// sensor location


// *** TIME SETTINGS ***
// Accurate time is necessary for certificate validation and writing in batches
// For the fastest time sync find NTP servers in your area: https://www.pool.ntp.org/zone/
// Set timezone string according to https://www.gnu.org/software/libc/manual/html_node/TZ-Variable.html
// Central Europe: "CET-1CEST,M3.5.0,M10.5.0/3"
#define TZ_INFO "CET-1CEST,M3.5.0,M10.5.0/3"

// measurement interval in [ms]
#define POLLING_INTERVAL 30000
#define TWAI_TIMEOUT 40000


// *** DEEP SLEEP ***
// activate "deep sleep" during idle resulting in low power consumtion
// |-> ! module will not be reachable during sleep
// |-> ! WIFI reconnect after every sleeping Phase -> short phase of high power consumption
// '-> result: activate deep sleep only for intervals longer then 5min
// #define DEEP_SLEEP


/* 
 * PIN OUT
 */
#define PIN_LED_BUILTIN 2
#define PIN_CAN_RX 4
#define PIN_CAN_TX 5
// #define CONFIG_CAN_SPEED TWAI_TIMING_CONFIG_500KBITS()
